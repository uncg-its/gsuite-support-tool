# G Suite Support Tool

A tool to help G Suite administrators with common/repetitive tasks via a GUI

# Instructions for Installation

Clone the repo

```
composer install
```

```
php artisan ccps:deploy
```

Update the .env file with the necessary google creds (downloading the json from the developer console)

```
$ vi .env
```

```
#GOOGLE CREDS
GOOGLE_APPLICATION_CREDENTIALS="./../google-creds.json"
GOOGLE_DOMAIN="yourdomain.edu"
GOOGLE_ADMIN_ACCOUNT="super_admin@yourdomain.edu"
```

```
php artisan db:seed --class="App\Seeders\GoogleRoleSeeder" && php artisan db:seed --class="App\Seeders\GooglePermissionSeeder"
```

Login to the app, add the permissions to the roles, and assign roles to the Admin user

Clear Cache, so the google menu shows up in the nav bar

If all works, you should be able to view users/groups via the menu
(http://app.whatever/google/groups)

# Version History

## 1.12.1

- Bugfix for replacing a group's membership

## 1.12.0

- Bulk update and delete users

## 1.11.0

- Group Presets implementation

## 1.10.3

- updates the Group CRUD to replace some deprecated Group flags (`whoCanAdd` and `showInGroupDirectory`)
- fixes an issue where editing a group may strip the last character of a group's email address (especially "-g" suffixes)

## 1.10.2

- fixes email filter listing on a user
- updates depenencies
- adjust code to account for new version of Google API wrapper's `listGroupMembers()` method change

## 1.10.1

- append search terms to groups list pagination

## 1.10

- CCPS Core 2.3

## 1.9.4

- Fixes and standardizations around usage of the `google.groups.show` route

## 1.9.3

- Splunk logging
- Fix for group list for user

## 1.9.2

- fix for bs4 forms `fill()` method

## 1.9.1

- Fix for group creation

## 1.9

- Adds local Google Group cache mechanism
- Groups controller is now more RESTful
- Laravel 8 support
- CCPS Core 2.2
- PHP8 readiness

## 1.8

- More email settings / information and group data
- Additional group and email functionality via API routes

## 1.7

- Adds API endpoint for whatis

## 1.6.1

- Fixes scopes for email settings

## 1.6

- Adds API endpoints for email filters and forwards

## 1.5.1

- Update to Laravel Framework ^7.22 (security patch)

## 1.5

- Adds "Replace membership" option to Google Group membership editor

## 1.4.1

- Fixes batch group membership operations so that the script does not abort on first failure, but instead returns a list of failed operations.

## 1.4

- Adds some proof-of-concept API Endpoints, primary purpose for CCPS Bot

## 1.3

- Adds Collaborative Inbox settings to group create/update/show

## 1.2

- CCPS Core 2.0
- Semantic versioning and real version tracking
- What Is tool

## 1.1.2 and 1.1.2.1

- Removed Compromise table and functions (replaced with simply Suspend function)

## 1.1.1

- Includes creation and updating of Google Groups when creating a "Program"
- Includes fixed to make sure Socialite/Azure plugin works, at least until CCPS-Core supports it out of the box

## 1.1

- Includes ACCESS_TYPE in the google env/config vars.
- Moves google creds to sub-folders

## 1.0

- Initial release
- Includes artisan app:deploy command
- Still requires manual association of permissions/roles to the admin user etc.
- Group creation & updates, user searching appear to work correctly
