@extends('layouts.wrapper', [
    'pageTitle' => 'Google Calendars'
])

@section('content')
    <h2>Google Calendars</h2>

    <p>
    USER: {{ $user->username }} // {{ $user->token }}
        <br />
    REQUEST-> CALENDAR_ID :@if(!empty($request)){{ $request->calendarId  }}@endif
    <br />
    THE SUBJECT: {{$theSubject}}
    <p>

    <form action="{{ route('google.calendars') }}" method="POST">

        <label for="calendarId">Calendar ID</label>
        <input type="text" class="form-control" id="calendarID" name="calendarId" placeholder="Calendar ID/Username etc" value="{{ request('calendarID')}}">

        {{ csrf_field() }}

        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>

    </form>
    @if(!empty($calendarListItems))

        <p>{{ count($calendarListItems) }} calendars found</p>
    <ol>
    @foreach ($calendarListItems as $cal)
        <li>{{ $cal->id }}   //  {{ $cal->summary }}  //  {{ $cal->accessRole }}</li>
    @endforeach
    </ol>
    @else
        <p>No Results currently.</p>
    @endif

@endsection()