@extends('layouts.wrapper', [
    'pageTitle' => 'What Is | Index'
])

@section('content')
    <div id="app">
        <h1>What Is</h1>
        <p>Use this tool to look up existing entities and determine their status. Good for identifying account types (primary, secondary, alias) and related information.</p>
        <hr>
        <div class="card mb-3">
            <div class="card-header">Search</div>
            <div class="card-body">
                <div class="form-inline">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" class="form-control mx-2" v-model="email" :disabled="lookup_in_progress" @keydown.enter="lookup">
                    <button class="btn btn-info btn-sm" @click.prevent="lookup" :disabled="lookup_in_progress">
                        <i class="fas fa-search"></i> Search
                    </button>
                </div>
            </div>
        </div>

        <div v-if="lookup_in_progress" class="mb-3">
            <div class="alert alert-warning">
                <i class="fas fa-spin fa-spinner"></i> Searching...
            </div>
        </div>

        <div v-if="thing">
            <div class="alert alert-info mb-3">
                <i class="fas fa-check"></i>
                <strong>@{{ searched }}</strong> is @{{ thing.prefix }} <span class="badge-pill badge-dark">@{{ thing.type }}</span> <span class="badge-pill badge-light" v-if="thing.subtype!==null">@{{ thing.subtype }}</span>
            </div>

            <whatis-user v-if="thing.type=='user'" :user="thing"></whatis-user>
            <whatis-user v-if="thing.type=='alias'" :user="thing"></whatis-user>
            <whatis-group v-if="thing.type=='group'" :group="thing"></whatis-group>
            {{-- <div class="bg-light p-3">
                <pre>@{{ thing }}</pre>
            </div> --}}
        </div>

        <div v-if="error">
            <div class="alert alert-danger">
                <i class="fas fa-ban"></i>
                The requested item does not exist in Google as a user, alias, or group.
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('js/gst.js') }}"></script>
@routes {{-- Ziggy --}}
<script>
    const app = new Vue({
        el: '#app',
        data: {
            error: null,
            thing: null,
            email: '',
            searched: '',
            lookup_in_progress: false,
        },
        methods: {
            lookup() {
                this.lookup_in_progress = true;
                this.error = null;
                this.thing = null;
                this.searched = this.email;

                axios
                    .post(route('google.whatis.search', {
                        email: this.email
                    }).url())
                    .then(response => {
                        // console.log(response);
                        this.thing = response.data.thing;
                    })
                    .catch(error => {
                        console.log(error);
                        this.error = 'Not found'
                    })
                    .finally(response => {
                        this.lookup_in_progress = false
                    });
            }
        }
    });
</script>
@append
