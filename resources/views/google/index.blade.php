
@extends('layouts.wrapper', [
    'pageTitle' => 'Google API | Index'
])

@section('content')
    {!! Breadcrumbs::render('google') !!}

    <h2>Google API</h2>
    <div class="row">
        @permission('gsuite.users.view')
        @include('components.panel-nav', [
            'url' => route('google.users.index'),
            'fa' => 'fas fa-user-circle',
            'title' => 'Users'
        ])
        @endpermission
        @permission('gsuite.groups.view')
        @include('components.panel-nav', [
            'url' => route('google.groups.index'),
            'fa' => 'fas fa-sitemap',
            'title' => 'Groups'
        ])
        @endpermission
        @permission('gsuite.whatis.view')
        @include('components.panel-nav', [
            'url' => route('google.whatis.index'),
            'fa' => 'fas fa-question-circle',
            'title' => 'What Is _____?'
        ])
        @endpermission
    </div>
    <hr>
    <h4>Settings & Information</h4>
    <div class="row">
        @permission('cache.*')
        @include('components.panel-nav', [
            'url' => route('cache'),
            'fa' => 'fas fa-database',
            'title' => 'API Cache'
        ])
        @endpermission
        {{-- @permission('gsuite.*')
        @include('components.panel-nav', [
            'url' => route('statistics'),
            'fa' => 'fas fa-chart-line',
            'title' => 'API Call Statistics'
        ])
        @endpermission --}}
    </div>
@endsection()
