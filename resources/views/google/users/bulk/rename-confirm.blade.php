@extends('layouts.wrapper', [
    'pageTitle' => 'Bulk Rename: Confirm'
])

@section('content')
    <h1>Bulk Rename: Confirm</h1>
    <p>Here is a sample of the what the email addrseses will look like with the proposed changes. Please confirm to continue.</p>
    @component('components.table')
        @slot('th')
            <th>Old Email</th>
            <th>New Email</th>
        @endslot
        @slot('tbody')
            @foreach (array_slice($newNames, 0, 10) as $oldName => $newName)
                <tr>
                    <td>{{ $oldName . '@' . config('google-api.domain') }}</td>
                    <td>{{ $newName . '@' . config('google-api.domain') }}</td>
                </tr>
            @endforeach
        @endslot
    @endcomponent

    {!! Form::route('google.users.bulk-rename-perform')->method('post')->open() !!}
        @foreach ($newNames as $oldName => $newName)
            {!! Form::hidden('names[' . $oldName . ']', $newName) !!}
        @endforeach
        <a type="button" href="{{ route('google.users.bulk-rename') }}" class="btn btn-danger"><i class="fas fa-ban"></i> Cancel</a>
        {!! Form::submit('<i class="fas fa-check"></i> Confirm, and perform in Google')->color('success') !!}
    {!! Form::close() !!}
@endsection
