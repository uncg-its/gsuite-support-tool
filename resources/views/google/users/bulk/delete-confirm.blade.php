@extends('layouts.wrapper', [
    'pageTitle' => 'Bulk Delete: Confirm'
])

@section('content')
    <h1>Bulk Delete: Confirm</h1>

    <p class="lead text-danger"><i class="fas fa-exclamation-triangle"></i> YOU ARE ABOUT TO DELETE {{ count($toDelete) }} ACCOUNT(S) PERMANENTLY. ARE YOU SURE?? <i class="fas fa-exclamation-triangle"></i></p>

    {!! Form::route('google.users.bulk-delete-perform')->method('post')->open() !!}
        @foreach ($toDelete as $username)
            {!! Form::hidden('names[]', $username) !!}
        @endforeach
        <a type="button" href="{{ route('google.users.bulk-delete') }}" class="btn btn-danger"><i class="fas fa-ban"></i> Cancel</a>
        {!! Form::submit('<i class="fas fa-check"></i> Confirm, and perform in Google')->color('success') !!}
    {!! Form::close() !!}
@endsection
