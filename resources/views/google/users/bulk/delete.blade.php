@extends('layouts.wrapper', [
    'pageTitle' => 'Bulk Delete'
])

@section('content')
    <h1>Bulk Delete users</h1>
    <p class="lead text-danger"><i class="fas fa-exclamation-triangle"></i> USE THIS FUNCTION WITH EXTREME CAUTION!! <i class="fas fa-exclamation-triangle"></i></p>
    <p>Use this function when you want to <strong>permanently delete</strong> Google accounts in bulk.</p>
    {!! Form::route('google.users.bulk-delete-confirm')->method('post')->multipart()->open() !!}
        <div class="row">
            <div class="col">
                {!! Form::file('csv', 'CSV File')->required(true)->help('CSV file containing a "username" column') !!}
            </div>
        </div>
        <div class="row">
            <div class="col">
                {!! Form::submit('Submit')->color('success') !!}
            </div>
        </div>
    {!! Form::close() !!}
@endsection
