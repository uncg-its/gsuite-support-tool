@extends('layouts.wrapper', [
    'pageTitle' => 'Bulk Rename'
])

@section('content')
    <h1>Bulk Rename users</h1>
    <p>Use this function when you want to rename user accounts (prefix or suffix) in bulk, to a similar format.</p>
    {!! Form::route('google.users.bulk-rename')->method('post')->multipart()->open() !!}
        <div class="row">
            <div class="col">
                {!! Form::file('csv', 'CSV File')->required(true)->help('CSV file containing a "username" column') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-6">{!! Form::text('prefix', 'Username Prefix')->help('The string to prefix the username with')->required(false) !!}</div>
            <div class="col-6">{!! Form::text('suffix', 'Username Suffix')->help('The string to suffix the username with')->required(false) !!}</div>
        </div>
        <div class="row">
            <div class="col">
                {!! Form::submit('Submit')->color('success') !!}
            </div>
        </div>
    {!! Form::close() !!}
@endsection
