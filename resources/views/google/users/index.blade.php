@extends('layouts.wrapper', [
    'pageTitle' => 'Google Users'
])

@section('content')
    {!! Breadcrumbs::render('google_users') !!}

    <div class="d-flex justify-content-between align-items-center">
        <h1>GSuite Users</h1>
        <div>
            @permission('gsuite.users.update')
                <a href="{{ route('google.users.bulk-rename') }}" class="btn btn-sm btn-warning"><i class="fas fa-i-cursor"></i> Bulk Rename</a>
            @endpermission
            @permission('gsuite.users.delete')
                <a href="{{ route('google.users.bulk-delete') }}" class="btn btn-sm btn-danger"><i class="fas fa-skull-crossbones"></i> Bulk Delete</a>
            @endpermission
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">Search / Filter</div>
                <div class="card-body">
                    <form action="{{ route('google.users.index') }}" method="POST">
                        {{ csrf_field() }}

                        <div class="form-row">
                            <div class="col-4">
                                <div class="form-group ">
                                 <label for="userSearch">Search</label>
                                 <input type="text" class="form-control" id="userSearch" name="userSearch" placeholder="Name, email" value="{{ request('userSearch')}}">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group ">

                                    <label for="formControlOrgUnit">Org Unit</label>
                                    <div class="col">
                                        <select class="form-control" id="formControlOrgUnit" name="OU">
                                            <option value="/">all</option>
                                            <option value="/fac-staff">/fac-staff</option>
                                            <option value="/students">/students</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group ">
                                    <span class="col">Limit</span>

                                    <div class="form-check col">
                                        <input name="isAdmin" class="form-check-input" type="checkbox">
                                        <label class="form-check-label badge badge-info" for="isAdmin">Administrator</label>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col-4 d-flex justify-content-start align-items-center">
                                <button class="btn btn-success btn-sm form-control-sm  mr-2" type="submit" >Submit</button>
                                <a class="btn btn-danger btn-sm form-control-sm" href="https://plx-ccps-webex.uncg.edu/webex-user-feed/feed/users" role="button" aria-disabled="" type="anchor">Reset</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <div class="mt-3">
        <div class="row">
            <div class="col-12">

                    @if(!empty($users))

                        <p>{{ count($users) }} Users returned. </p>

                        <p><a class="btn btn-sm btn-warning" href="{{ route('google.users.index') }}?go={{ $nextPageToken }}"><i class="fa fa-arrow-right"></i> Next Page</a></p>
                    @else
                    <p>No users returned.</p>
                    @endif
            </div>
        </div>

                    <table class="table table-sm table-hover">
                     <thead class="thead-light">
                            <tr>
                                <th>Admin</th>
                                <th>Email</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Org Unit</th>
                                <th></th>Status</th>

                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($users as $thisUser)
                                <tr>
                                    <td>@if ($thisUser->isAdmin == 1) <span class="badge badge-info">Administrator</span> @endif</td>

                                    <td>{{ $thisUser->primaryEmail }}</td>

                                    <td>{{ $thisUser->name->givenName }}</td>
                                    <td>{{ $thisUser->name->familyName }}</td>
                                    <td>{{ $thisUser->orgUnitPath }}  </td>
                                    @if ($thisUser->suspended == 1)
                                        <td class="text-danger"><i class="fas fa-times"></i> suspended</td>
                                    @else
                                        <td class="text-success"><i class="fas fa-check"></i> active</td>
                                    @endif


                                    <td>
                                        <a href="{{ route('google.users.key', ['key' => $thisUser->id]) }}" class="btn btn-primary btn-sm"><i class="fas fa-list"></i> Details</a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                </div>

@endsection()
