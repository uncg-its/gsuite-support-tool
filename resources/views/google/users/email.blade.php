@extends('layouts.wrapper', [
    'pageTitle' => 'Google User Email Settings'
])
@section('content')

    @if(!empty($userInfo))

    {!! Breadcrumbs::render('google_users_show') !!}

    <h1>Google User Email Configuration</h1>

        <div class="card mb-3 @if ($userInfo->suspended == 1)  border-danger @endif">
            <h5 class="card-header">User Information</h5>
            <div class="card-body">


                <div class="row">
                    <div class="col-1">
                        <img  class="rounded-circle d-block mx-auto img-fluid" src="@if(!empty($userPhoto)) {{$userPhoto}} @else {{ asset('images/default-avatar.jpg') }} @endif" width="96" style="float:left;margin-right:10px;"/>
                    </div>
                    <div class="col">
                        <h2>{{ $userInfo->name->fullName }}
                            @if ($userInfo->suspended == 1) <span class="badge badge-danger"><i class="fas fa-lock"></i> suspended</span> @endif
                            @if ($userInfo->isAdmin == 1) <span class="badge badge-info"><i class="fas fa-star"></i> admin</span></td> @endif
                        </h2>
                        {{ $userInfo->orgUnitPath }}
                        <h3>{{ $userInfo->primaryEmail }}</h3>
                        <h5>Aliases</h5>
                        @if(!empty($userAliases))
                            <ul>
                                @foreach ($userAliases as $alias)
                                    <li>{{ $alias['alias'] }}</li>
                                @endforeach
                            </ul>
                        @else
                            <p><em>No aliases found for this user.</em></p>
                        @endif

                        


                    </div>
                    <div class="col">
                        <h4>Helpful Support Links</h4>

                    <ul>
                        <li><a href="https://support.google.com/mail/answer/6562">Change your Gmail settings<br />
                            https://support.google.com/mail/answer/6562</a></li>
                    </ul>
                        
                    <h5>FYI</h5>
                    <ul>
                        <li>Email Language: {{ $emailLanguage->displayLanguage }}</li>
                        <li>Total Messages: {{ $emailProfile->messagesTotal}}</li>
                        <li>Total Threads: {{ $emailProfile->threadsTotal}}</li>

                    </ul>

                    </div>
                </div>
            </div>
        </div>
        


        <div class="card mb-3">
            <div class="card-header">
                <h5>Send As</h5>
            </div>
            <div class="card-body">
                <p>Number of Send As: {{ count($emailSendAs) }}</p>
                @if(!empty($emailSendAs))
                <table class="table table-sm table-hover">
                    <caption>List of SendAs Configurations</caption>
                    <thead>
                      <tr>
                        <th scope="col">Email</th>
                        <th scope="col">Display Name</th>
                        <th scope="col">Reply To Address</th>
                        <th scope="col">Verification</th>
                        <th scope="col">Signature</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($emailSendAs as $sendAs)
                        <tr>
                            <th scope="row">{{ $sendAs->sendAsEmail }}
                                @if ($sendAs->isPrimary == 1) <span class="badge badge-warning">Primary</span> @endif
                                @if ($sendAs->isDefault == 1) <span class="badge badge-warning">Default</span> @endif
                                @if ($sendAs->treatAsAlias == 1) <span class="badge badge-warning">Treat As Alias</span> @endif
                            
                            </th>
                            <td>{{ $sendAs->displayName }}</td>
                            <td>{{ $sendAs->replyToAddress }}</td>
                            <td>
                                @if (!$sendAs->isPrimary == 1)
                                    @if ($sendAs->verificationStatus == 'accepted') 
                                    <span class="badge badge-success">{{ $sendAs->verificationStatus }}</span> 
                                    @else 
                                    <span class="badge badge-danger">{{ $sendAs->verificationStatus }}</span> 

                                    @endif
                                @endif

                                
                            </td>
                            <td>{{ $sendAs->signature }}</td>
                          </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                <p><em>No forwards found for this user.</em></p>
                @endif
            </div>
            <div class="card-footer text-muted">
                <a href="https://support.google.com/mail/answer/22370">https://support.google.com/mail/answer/22370</a>
            </div>
        </div>
        
        

                <div class="card mb-3">
                    <div class="card-header">
                        <h5>Forwards</h5>
                    </div>
                    <div class="card-body">
                        <p>Number of Forwards: {{ count($emailForwards) }}</p>
                        @if(!empty($emailForwards))
                            <ul>
                                @foreach ($emailForwards as $forward)
                                    <li><strong>{{ $forward->forwardingEmail }}</strong><br />
                                        Verfication Status: 
                                        @if($forward->verificationStatus == 'accepted') 
                                            <span style="color:green">{{ $forward->verificationStatus }}</span>
                                        @else
                                            <span style="color:red">{{ $forward->verificationStatus }}</span>
                                        @endif
                                        </li>
                                @endforeach
                            </ul>
                        @else
                        <p><em>No forwards found for this user.</em></p>
                        @endif




                    </div>
                    <div class="card-footer text-muted">
                        <a href="https://support.google.com/mail/answer/10957">https://support.google.com/mail/answer/10957</a>
                    </div>
                </div>
           
                <div class="card mb-3">

                    <div class="card-header">
                        <h5>Delegates</h5>
                    </div>
                    <div class="card-body">
                        <p>Number of Delegations: {{ count($emailDelegates) }}</p>
                        @if(!empty($emailDelegates))
                            <ul>
                                @foreach ($emailDelegates as $delegate)
                                <li><strong>{{ $delegate->delegateEmail }}</strong><br />
                                    Verfication Status: 
                                    @if($delegate->verificationStatus == 'accepted') 
                                    <span style="color:green">{{ $delegate->verificationStatus }}</span>
                                    @else
                                    <span style="color:red">{{ $delegate->verificationStatus }}</span>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                        @else
                        <p><em>No delegates found for this user.</em></p>
                        @endif
                    </div>
                    <div class="card-footer text-muted">
                        <a href="https://support.google.com/mail/answer/138350">https://support.google.com/mail/answer/138350</a>
                    </div>
                </div>
            
                <div class="card mb-3">

                    <div class="card-header">
                        <h5>Out of Office / Vacation 
                            @if ($emailVacation->enableAutoReply==1) 
                            <span class="badge badge-success">Enabled</span> 
                        @else 
                            <span class="badge badge-danger">Disabled</span> 
                        @endif
                        </h5>
                    </div>
                    <div class="card-body">                
                        @if(!empty($emailVacation))
                             <div class="row">
                                 <div class="col-6">
                                    <p>Duration: {{ gmdate('M j, Y', floor($emailVacation->startTime/1000)) }} through  {{ gmdate('M j, Y', floor($emailVacation->endTime/1000)) }}</p>
                                    <ul>
                                        <li>Restrict to Contacts: @if ($emailVacation->restrictToContacts == 1) Yes @else No @endif</li>
                                        <li><span data-toggle="tooltip" title="determines whether responses are sent to recipients who are outside of the user's domain.">Restrict to Domain:</span> @if ($emailVacation->restrictToDomain == 1) Yes @else No @endif</li>
                                    </ul>
                                 </div>
                                 <div class="col-6">
                                <p>Subject: {{ $emailVacation->responseSubject}}</p>
                                <p>Message
                                @if (!empty($emailVacation->responseBodyPlainText))
                                (Plain Text)
                                @elseif (!empty($emailVacation->responseBodyHtml))
                                (Rich Format)
                                @endif
                                    :<br />
                                {{$emailVacation->responseBodyPlainText }}
                                @if (!empty($emailVacation->responseBodyHtml))
                                
                                {{ html_entity_decode($emailVacation->responseBodyHtml) }}
                                <p><em>NOTE: This is the HTML code that is stored.<br />
                                    The is NOT as user sees it in the vacation settings interface.</em></p>
                                @endif
                                </div>
                            </div>   
                        @else
                            <p><em>No Vacation setting found for thus user.</em></p>
                        @endif 
                    </div> 
                    <div class="card-footer text-muted">
                        <a href="https://support.google.com/mail/answer/25922">https://support.google.com/mail/answer/25922</a>
                    </div>

            </div>
   
   
            <div class="card mb-3">

                <div class="card-header">
                    <h5>Labels</h5>
                </div>
                <div class="card-body">
                    <p>Number of Labels: {{ count($emailLabels) }}</p>
                    <table class="table table-sm table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Type</th>
                            <th scope="col">Label</th>
                            <th scope="col">Id</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($emailLabels as $label)
                          <tr>
                            <th scope="row">{{ $label->type }}</th>
                            <td>{{ $label->name }}</td>
                            <td>{{ $label->id }}</td>
                          </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="card-footer text-muted">
                    <a href="https://support.google.com/mail/answer/118708">https://support.google.com/mail/answer/118708</a>
                </div>
            </div>
        
            <div class="card mb-3">
                <div class="card-header">
                    <h5>Filters</h5>
                </div>
                <div class="card-body">
                @if(!empty($emailFilters))
                    <p>Number of Filters: {{ count($emailFilters) }}</p>

                    

                        @foreach ($emailFilters as $thisFilter)
                        <div class="row">
                            <div class="col">
                                ID: <em>{{ $thisFilter->id }}</em><br />
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-4">
                                    Matches: 
                                    <ul>
                                    @if($thisFilter->criteria->query !=null)
                                    <li><strong>Has the words: '{{ $thisFilter->criteria->query }}'</strong></li>
                                    @endif
                                    @if($thisFilter->criteria->negatedQuery !=null)
                                    <li><strong>Doesn't have the words: '{{ $thisFilter->criteria->negatedQuery }}'</strong></li>
                                    @endif
                                    @if($thisFilter->criteria->hasAttachment !=null)
                                    <li><strong>Has: attachment</strong></li>
                                    @endif
                                    @if($thisFilter->criteria->from !=null)
                                    <li><strong>From: {{ $thisFilter->criteria->from }}</strong></li>
                                    @endif
                                    @if($thisFilter->criteria->to !=null)
                                    <li><strong>To: {{ $thisFilter->criteria->to}}</strong></li>
                                    @endif
                                    @if($thisFilter->criteria->subject !=null)
                                    <li><strong>Subject: {{ $thisFilter->criteria->subject }}</strong></li>
                                    @endif
                                    </ul>
                            </div>
                            <div class="col">    
                                    Do This:
                                    <ul>
                                    @if($thisFilter->action->forward !=null)
                                    <li><strong>Forward to: {{ $thisFilter->action->forward }}</strong></li>
                                    @endif
                                    
                                    @if($thisFilter->action->addLabelIds !=null)
                                    <li><strong>Add Label(s):{{ implode(', ', $thisFilter->action->addLabelIds) }}</strong></li>
                                    @endif
                                    @if($thisFilter->action->removeLabelIds !=null)
                                    <li><strong>Remove Label(s):{{ implode(', ', $thisFilter->action->removeLabelIds) }}</strong></li>
                                    @endif
                                
                                    </ul>
                            </div>
                        </div>
                        @endforeach
                    
                @else
                    <p>No Filters.</p>
                @endif
                </div>
                <div class="card-footer text-muted">
                    <a href="https://support.google.com/mail/answer/6579">https://support.google.com/mail/answer/6579</a>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="card mb-3">
                        <div class="card-header">
                            <h5>POP</h5>
                        </div>
                        <div class="card-body">
    
                            Access Window: <strong>{{ $emailPop->accessWindow}} </strong>
    
                            <table class="table table-sm table-hover">
                                <caption>POP Access Window Configuration Reference</caption>
                                <thead>
                                  <tr>
                                    <th scope="col">Value</th>
                                    <th scope="col">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <tr><td>ACCESS_WINDOW_UNSPECIFIED</td><td>Unspecified range.</td></tr>
                                <tr><td>DISABLED</td><td>Indicates that no messages are accessible via POP.</td></tr>
                                <tr><td>FROM_NOW_ON</td><td>Indicates that unfetched messages received after some past point in time are accessible via POP.</td></tr>
                                 <tr><td>ALL_MAIL</td><td>Indicates that all unfetched messages are accessible via POP.</td></tr>
                        </tbody>
                    </table>
    
                            Disposition: <strong>{{ $emailPop->disposition}} </strong>
    
    
                            <table class="table table-sm table-hover">
                                <caption>POP Disposition Configuration Reference</caption>
                                <thead>
                                <tr>
                                    <th scope="col">Value</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr><td>DISPOSITION_UNSPECIFIED</td><td>Unspecified disposition.</td></tr>
                                <tr><td>LEAVE_IN_INBOX</td><td>Leave the message in the INBOX.</td></tr>
                                <tr><td>ARCHIVE</td><td>Archive the message.</td></tr>
                                <tr><td>TRASH</td><td>Move the message to the TRASH.</td></tr>
                                <tr><td>MARK_READ</td><td>Leave the message in the INBOX and mark it as read.</td></tr>
                                </tbody>
                                </table>
    
                        </div>
                        <div class="card-footer text-muted">
                            POP: <a href="https://support.google.com/mail/answer/7104828">https://support.google.com/mail/answer/7104828</a><br />
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-3">
                        <div class="card-header">
                            <h5>IMAP @if ($emailImap->enabled==1) 
                                <span class="badge badge-success">Enabled</span> 
                            @else 
                                <span class="badge badge-danger">Disabled</span> 
                            @endif</h5>
                        </div>
                        <div class="card-body">
                                <ul>
                                    <li>Auto Expunge:  <strong>@if ($emailImap->autoExpunge==1) On @else Off @endif</strong></li>
                                    <li>Max Folder Size: <strong>{{ $emailImap->maxFolderSize }}</strong></li>
                                    <li>Expunge Behavior: <strong>{{ $emailImap->expungeBehavior }}</strong></li>
                                </ul>
                       
                                <table class="table table-sm table-hover">
                                    <caption>IMAP Expunge Behavior Configuration Reference</caption>
                                    <thead>
                                      <tr>
                                        <th scope="col">Value</th>
                                        <th scope="col">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                <tr><td>EXPUNGE_BEHAVIOR_UNSPECIFIED</td><td>Unspecified behavior.</td></tr>
                                <tr><td>ARCHIVE</td><td>Archive messages marked as deleted.</td></tr>
                                <tr><td>TRASH</td><td>Move messages marked as deleted to the trash.</td></tr>
                                <tr><td>DELETE_FOREVER</td><td>Immediately and permanently delete messages marked as deleted. The expunged messages cannot be recovered.</td></tr>
                                    </tbody>
                            </table>
    
                    
                        </div>
                        <div class="card-footer text-muted">
                            IMAP <a href="https://support.google.com/mail/answer/7126229">https://support.google.com/mail/answer/7126229</a>
                        </div>
                    </div>
                </div>
            </div>


    @else
        <p>ISSUE: No user defined.</p>
        <p><a href="{{ URL::previous() }}" class="btn btn-warning">
            <i class="fa fa-arrow-left"></i> Go Back
        </a></p>
    @endif

@endsection()
