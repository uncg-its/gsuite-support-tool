@extends('layouts.wrapper', [
    'pageTitle' => 'Google User Detail'
])
@section('content')
    {!! Breadcrumbs::render('google_users_show') !!}

    <h1>Google User Details</h1>
    @if(!empty($userInfo))

        <div class="card mb-3 @if ($userInfo->suspended == 1)  border-danger @endif">
            <h5 class="card-header">User Information</h5>
            <div class="card-body">


                <div class="row">
                    <div class="col-1">
                        <img  class="rounded-circle d-block mx-auto img-fluid" src="@if(!empty($userPhoto)) {{$userPhoto}} @else {{ asset('images/default-avatar.jpg') }} @endif" width="96" style="float:left;margin-right:10px;"/>
                    </div>
                    <div class="col">
                        <h2>{{ $userInfo->name->fullName }}
                            @if ($userInfo->suspended == 1) <span class="badge badge-danger"><i class="fas fa-lock"></i> suspended</span> @endif
                            @if ($userInfo->isAdmin == 1) <span class="badge badge-info"><i class="fas fa-star"></i> admin</span></td> @endif
                        </h2>
                        {{ $userInfo->orgUnitPath }}
                        <h3>{{ $userInfo->primaryEmail }}</h3>
                        <h5>Aliases</h5>
                        @if(!empty($userAliases))
                            <ul>
                                @foreach ($userAliases as $alias)
                                    <li>{{ $alias['alias'] }}</li>
                                @endforeach
                            </ul>
                        @else
                            <p><em>No aliases found for this user.</em></p>
                        @endif
                        <p> Created: {{ $userInfo->creationTime }}</p>
                        <p> Last Login: {{ $userInfo->lastLoginTime }}</p>
                        @permission('gsuite.users.delete')
                        <form method="POST" action="{{ route('google.users.suspend', ['suspend' => $userInfo->id]) }}">
                            <button type="submit" class="btn btn-danger" @if ($userInfo->suspended == 1)  disabled @endif>Suspend User</button>
                            {{ csrf_field() }}
                        </form>
                        @endpermission
                    </div>
                    <div class="col">
                        <h5>Tokens</h5>
                            @if(!empty($userTokens))
                                <p>One or more tokens exist!</p>
                                <ul>
                                    @foreach ($userTokens as $token)
                                        <li>{{ $token['displayText'] }}</li>
                                    @endforeach
                                </ul>
                            @else
                            <p><em>No tokens found for this user.</em></p>
                            @endif
                    </div>
                </div>
            </div>
        </div>
 
     
        <div class="row">
            @permission('gsuite.users.view')
            @include('components.panel-nav', [
                'url' => route('google.users.email',['key' => $userInfo->id]),
                'fa' => 'fas fa-envelope',
                'title' => 'Email Settings'
            ])
            @endpermission
            @permission('gsuite.groups.view')
            @include('components.panel-nav', [
                'url' => route('google.users.groups',['key' => $userInfo->id]),
                'fa' => 'fas fa-sitemap',
                'title' => count($userGroups) .' Groups'
            ])
            @endpermission
            
        </div>
    


    
    @else
        <p>No user info returned.</p>
        <p><a href="{{ URL::previous() }}" class="btn btn-warning">
            <i class="fa fa-arrow-left"></i> Go Back
        </a></p>
    @endif

@endsection()
