@extends('layouts.wrapper', [
    'pageTitle' => 'Google Group: Edit Settings'
])

@section('content')
    {!! Breadcrumbs::render('google_groups_edit') !!}
    <h1>Google Group :: Edit Settings</h1>
    @if(!empty($groupSettings))
    <form method="POST" action="{{ route('google.groups.update', $group) }}" enctype="application/x-www-form-urlencoded">
        {{ csrf_field() }}

    <div class="card mb-3">

            <h5 class="card-header">Group Information</h5>
            <div class="card-body">


                <div class="form-group row">
                    <label for="groupName" class="col-sm-3 col-form-label">Group Name <span class="badge badge-danger">Required</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="groupName" id="groupName" value="{{ $groupSettings->name }}" required="required" class="form-control" aria-describedby="groupNameHelpBlock">
                        <small id="groupNameHelpBlock" class="form-text text-muted">Succinct display name of the Google Group.</small>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="groupDescription" class="col-sm-3 col-form-label">Group Description <span class="badge badge-danger">Required</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="groupDescription" id="groupDescription" value="{{ $groupSettings->description }}" required="required" class="form-control">
                        <small id="groupDescriptionHelpBlock" class="form-text text-muted">Description for this Google Group</small>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="groupEmail" class="col-sm-3 col-form-label">Group Email Prefix <span class="badge badge-danger">Required</span></label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <input type="hidden" name="originalGroupEmail" id="originalGroupEmail" value="{{ explode('@', $groupSettings->email)[0] }}" append="@uncg.edu" required="required" class="form-control">
                            <input type="text" name="groupEmail" id="groupEmail" value="{{ explode('@', $groupSettings->email)[0] }}" append="@uncg.edu" required="required" class="form-control" aria-describedby="groupEmailHelpBlock">
                            <div class="input-group-append">
                                <span class="input-group-text">{{ '@'. config('google-api.domain') }}</span>
                            </div>
                        </div>
                        <small id="groupEmailHelpBlock" class="form-text text-muted">Group Email Prefix for this Google Group</small>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="groupSettingsFieldset-setWhoCanModerateMembers" class="col-sm-3 col-form-label">setWhoCanModerateMembers</label>
                    <div class="col-sm-9">
                        <select name="groupSettingsFieldset[setWhoCanModerateMembers]" id="groupSettingsFieldset-setWhoCanModerateMembers" class="">
                            @foreach ($groupSettingsPossibleValues['whoCanModerateMembers'] as $thisWhoCanModerateMembers)
                                <option value="{{ $thisWhoCanModerateMembers }}" @if($thisWhoCanModerateMembers == $groupSettings->whoCanModerateMembers) selected="selected" @endif>{{ $thisWhoCanModerateMembers }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanJoin" class="col-sm-3 col-form-label">setWhoCanJoin</label>
                    <div class="col-sm-9">
                        <select name="groupSettingsFieldset[setWhoCanJoin]" id="groupSettingsFieldset-setWhoCanJoin" class="">
                            @foreach ($groupSettingsPossibleValues['whoCanJoin'] as $thisWhoCanJoin)
                                <option value="{{ $thisWhoCanJoin }}" @if($thisWhoCanJoin == $groupSettings->whoCanJoin) selected="selected" @endif>{{ $thisWhoCanJoin }}</option>
                            @endforeach
                        </select></div></div>
                <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanLeaveGroup" class="col-sm-3 col-form-label">setWhoCanLeaveGroup</label>
                    <div class="col-sm-9">
                        <select name="groupSettingsFieldset[setWhoCanLeaveGroup]" id="groupSettingsFieldset-setWhoCanLeaveGroup" class="">
                            @foreach ($groupSettingsPossibleValues['whoCanLeaveGroup'] as $thisWhoCanLeaveGroup)
                                <option value="{{ $thisWhoCanLeaveGroup }}" @if($thisWhoCanLeaveGroup == $groupSettings->whoCanLeaveGroup) selected="selected" @endif>{{ $thisWhoCanLeaveGroup }}</option>
                            @endforeach
                        </select></div></div>
                <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanViewMembership" class="col-sm-3 col-form-label">setWhoCanViewMembership</label>
                    <div class="col-sm-9">
                        <select name="groupSettingsFieldset[setWhoCanViewMembership]" id="groupSettingsFieldset-setWhoCanViewMembership" class="">
                            @foreach ($groupSettingsPossibleValues['whoCanViewMembership'] as $thisWhoCanViewMembership)
                                <option value="{{ $thisWhoCanViewMembership }}" @if($thisWhoCanViewMembership == $groupSettings->whoCanViewMembership) selected="selected" @endif>{{ $thisWhoCanViewMembership }}</option>
                            @endforeach
                        </select></div></div>
                <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanViewGroup" class="col-sm-3 col-form-label">setWhoCanViewGroup</label>
                    <div class="col-sm-9">
                        <select name="groupSettingsFieldset[setWhoCanViewGroup]" id="groupSettingsFieldset-setWhoCanViewGroup" class="">
                            @foreach ($groupSettingsPossibleValues['whoCanViewGroup'] as $thisWhoCanViewGroup)
                                <option value="{{ $thisWhoCanViewGroup }}" @if($thisWhoCanViewGroup == $groupSettings->whoCanViewGroup) selected="selected" @endif>{{ $thisWhoCanViewGroup }}</option>
                            @endforeach
                        </select></div></div>
                <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanInvite" class="col-sm-3 col-form-label">setWhoCanInvite</label>
                    <div class="col-sm-9">
                        <select name="groupSettingsFieldset[setWhoCanInvite]" id="groupSettingsFieldset-setWhoCanInvite" class="" aria-describedby="groupSettingsFieldset-setWhoCanInvite-HelpBlock">
                            @foreach ($groupSettingsPossibleValues['whoCanInvite'] as $thisWhoCanInvite)
                                <option value="{{ $thisWhoCanInvite }}" @if($thisWhoCanInvite == $groupSettings->whoCanInvite) selected="selected" @endif>{{ $thisWhoCanInvite }}</option>
                            @endforeach
                        </select>
                        <small id="groupSettingsFieldset-setWhoCanInvite-HelpBlock" class="form-text text-muted">What members have the ability to invite others to the Group.</small>

                    </div></div>
                <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanPostMessage" class="col-sm-3 col-form-label">setWhoCanPostMessage</label>
                    <div class="col-sm-9">
                        <select name="groupSettingsFieldset[setWhoCanPostMessage]" id="groupSettingsFieldset-setWhoCanPostMessage" class="">
                            @foreach ($groupSettingsPossibleValues['whoCanPostMessage'] as $thisWhoCanPostMessage)
                                <option value="{{ $thisWhoCanPostMessage }}" @if($thisWhoCanPostMessage == $groupSettings->whoCanPostMessage) selected="selected" @endif>{{ $thisWhoCanPostMessage }}</option>
                            @endforeach
                        </select></div></div>
                <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanContactOwner" class="col-sm-3 col-form-label">setWhoCanContactOwner</label>
                    <div class="col-sm-9">
                        <select name="groupSettingsFieldset[setWhoCanContactOwner]" id="groupSettingsFieldset-setWhoCanContactOwner" class="">
                            @foreach ($groupSettingsPossibleValues['whoCanContactOwner'] as $thisWhoCanContactOwner)
                                <option value="{{ $thisWhoCanContactOwner }}" @if($thisWhoCanContactOwner == $groupSettings->whoCanContactOwner) selected="selected" @endif>{{ $thisWhoCanContactOwner }}</option>
                            @endforeach
                        </select></div></div>
                <div class="form-group row"><label for="groupSettingsFieldset-setIsArchived" class="col-sm-3 col-form-label">setIsArchived</label>
                    <div class="col-sm-9">
                        @if ($groupSettings->isArchived == "true")
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="groupSettingsFieldset[setIsArchived]" id="groupSettingsFieldset-setIsArchived-true" value="true"   checked="checked" aria-describedby="groupSettingsFieldset-setIsArchived-HelpBlock">
                            <label class="form-check-label" for="groupSettingsFieldset-setIsArchived-true">Yes // True</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="groupSettingsFieldset[setIsArchived]" id="groupSettingsFieldset-setIsArchived-false" value="false" aria-describedby="groupSettingsFieldset-setIsArchived-HelpBlock">
                            <label class="form-check-label" for="groupSettingsFieldset-setIsArchived-false">No // False</label>
                        </div>
                        @else
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setIsArchived]" id="groupSettingsFieldset-setIsArchived-true" value="true"  aria-describedby="groupSettingsFieldset-setIsArchived-HelpBlock">
                                <label class="form-check-label" for="groupSettingsFieldset-setIsArchived-true">Yes // True</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setIsArchived]" id="groupSettingsFieldset-setIsArchived-false" value="false"   checked="checked" aria-describedby="groupSettingsFieldset-setIsArchived-HelpBlock">
                                <label class="form-check-label" for="groupSettingsFieldset-setIsArchived-false">No // False</label>
                            </div>
                        @endif
                        <small id="groupSettingsFieldset-setIsArchived-HelpBlock" class="form-text text-muted">Controls if group messages are archived. If disabled, previously received messages will remain archived but new messages will not be available on the web. Disabling archiving will also disable digest email mode</small>
                    </div></div>
                <div class="form-group row"><label for="groupSettingsFieldset-setAllowExternalMembers" class="col-sm-3 col-form-label">setAllowExternalMembers</label>
                    <div class="col-sm-9">
                        @if ($groupSettings->allowExternalMembers == "true")
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setAllowExternalMembers]" id="groupSettingsFieldset-setAllowExternalMembers-true" value="true"   checked="checked" aria-describedby="groupSettingsFieldset-setAllowExternalMembers-HelpBlock">
                                <label class="form-check-label" for="groupSettingsFieldset-setAllowExternalMembers-true">Yes // True</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setAllowExternalMembers]" id="groupSettingsFieldset-setAllowExternalMembers-false" value="false" aria-describedby="groupSettingsFieldset-setAllowExternalMembers-HelpBlock">
                                <label class="form-check-label" for="groupSettingsFieldset-setAllowExternalMembers-false">No // False</label>
                            </div>
                        @else
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setAllowExternalMembers]" id="groupSettingsFieldset-setAllowExternalMembers-true" value="true" aria-describedby="groupSettingsFieldset-setAllowExternalMembers-HelpBlock">
                                <label class="form-check-label" for="groupSettingsFieldset-setAllowExternalMembers-true">Yes // True</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setAllowExternalMembers]" id="groupSettingsFieldset-setAllowExternalMembers-false" value="false"  checked="checked" aria-describedby="groupSettingsFieldset-setAllowExternalMembers-HelpBlock">
                                <label class="form-check-label" for="groupSettingsFieldset-setAllowExternalMembers-false">No // False</label>
                            </div>
                        @endif
                        <small id="groupSettingsFieldset-setAllowExternalMembers-HelpBlock" class="form-text text-muted">May this group contain members not of the domain {{ '@'. config('google-api.domain') }}?</small>

                    </div>
                </div>
                <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanDiscoverGroup" class="col-sm-3 col-form-label">setWhoCanDiscoverGroup</label>
                    <div class="col-sm-9">
                        <select name="groupSettingsFieldset[setWhoCanDiscoverGroup]" id="groupSettingsFieldset-setWhoCanDiscoverGroup" class="">
                            @foreach ($groupSettingsPossibleValues['whoCanDiscoverGroup'] as $thisWhoCanDiscoverGroup)
                                <option value="{{ $thisWhoCanDiscoverGroup }}" @if($thisWhoCanDiscoverGroup == $groupSettings->whoCanDiscoverGroup) selected="selected" @endif>{{ $thisWhoCanDiscoverGroup }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="groupSettingsFieldset-setIncludeInGlobalAddressList" class="col-sm-3 col-form-label">setIncludeInGlobalAddressList</label>
                    <div class="col-sm-9">
                        @if ($groupSettings->includeInGlobalAddressList == "true")

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="groupSettingsFieldset[setIncludeInGlobalAddressList]" id="groupSettingsFieldset-setIncludeInGlobalAddressList-true" value="true"  checked="checked">
                            <label class="form-check-label" for="groupSettingsFieldset-setIncludeInGlobalAddressList-true">Yes // True</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="groupSettingsFieldset[setIncludeInGlobalAddressList]" id="groupSettingsFieldset-setIncludeInGlobalAddressList-false" value="false">
                            <label class="form-check-label" for="groupSettingsFieldset-setIncludeInGlobalAddressList-false">No // False</label>
                        </div>
                        @else
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setIncludeInGlobalAddressList]" id="groupSettingsFieldset-setIncludeInGlobalAddressList-true" value="true">
                                <label class="form-check-label" for="groupSettingsFieldset-setIncludeInGlobalAddressList-true">Yes // True</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setIncludeInGlobalAddressList]" id="groupSettingsFieldset-setIncludeInGlobalAddressList-false" value="false" checked="checked">
                                <label class="form-check-label" for="groupSettingsFieldset-setIncludeInGlobalAddressList-false">No // False</label>

                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="groupSettingsFieldset-setEnableCollaborativeInbox" class="col-sm-3 col-form-label">setEnableCollaborativeInbox</label>
                    <div class="col-sm-9">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="groupSettingsFieldset[setEnableCollaborativeInbox]" id="groupSettingsFieldset-setEnableCollaborativeInbox-true" value="true" @if ($groupSettings->enableCollaborativeInbox) checked="checked" @endif >
                            <label class="form-check-label" for="groupSettingsFieldset-setEnableCollaborativeInbox-true">Yes // True</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="groupSettingsFieldset[setEnableCollaborativeInbox]" id="groupSettingsFieldset-setEnableCollaborativeInbox-false" value="false" @if (!$groupSettings->enableCollaborativeInbox) checked="checked" @endif >
                            <label class="form-check-label" for="groupSettingsFieldset-setEnableCollaborativeInbox-false">No // False</label>
                        </div>
                        <small id="groupSettingsFieldset-setEnableCollaborativeInbox-HelpBlock" class="form-text text-muted">Is this group a Collaborative Inbox?</small>

                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Submit</button>
                            <input type="hidden" name="groupId" id="groupId" value=" {{ $group->id }}">
                            <a href="{{ route('google.groups.show', $group) }}" class="btn btn-danger"><i class="fas fa-times"></i> Cancel</a>
                        </div>
                    </div>
                </div>



            </div>
        @else
            <p>An error occurred: No Group Setting information provided to update.</p>
        @endif
    </div>
    </form>



@endsection()
