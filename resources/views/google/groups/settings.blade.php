@extends('layouts.wrapper', [
    'pageTitle' => 'Modify Google Group Settings'
])

@section('content')
    <h1>Modify Google Group Settings</h1>

    @if(!empty($groupSettings))
        <h2>{{ $groupSettings->name }} // {{ $groupSettings->email }}</h2>

        <p>Description:
            @if(!empty($groupSettings->description)) {{ $groupSettings->description }} @else <em>no description provided</em>@endif</p>
        <h4>Settings:</h4>
<ul>
    <li>Who Can Join: {{ $groupSettings->whoCanJoin }} </li>
    <li>Allow External Members?  @if ($groupSettings->allowExternalMembers == true)
            YES
        @else
            NO
        @endif
    </li>
    <li>Who Can Invite: {{ $groupSettings->whoCanInvite }} </li>
    <li>Who Can Add: {{ $groupSettings->whoCanAdd }} </li>
    <li>Who Can Leave: {{ $groupSettings->whoCanLeaveGroup }} </li>

    <li>Who Can View Memberships: {{ $groupSettings->whoCanViewMembership }} </li>
    <li>Who Can View Group: {{ $groupSettings->whoCanViewGroup }} </li>
    <li>Who Can Post Messages: {{ $groupSettings->whoCanPostMessage }} </li>
    <li>Allow Web Postings?  @if ($groupSettings->allowWebPostings == true)
            YES
        @else
            NO
        @endif
    </li>
</ul>
    @else
        <p>No Group Information/Settings returned.</p>
    @endif

    <form id="createGroupForm" enctype="application/x-www-form-urlencoded" class="form-horizontal" action="" method="post">
        <div class="control-group"><label for="groupEmail" class="control-label optional">Group Email Address</label>
            <div class="controls">
                <textarea name="groupEmail" id="groupEmail" style="width: 50%; height: 150px;" class="" rows="24" cols="80"></textarea>
                <p class="help-block">Enter each Group Email on a line by itself</p></div></div>
        <div class="control-group"><label for="groupSettingsFieldset-setWhoCanAdd" class="control-label optional">setWhoCanAdd</label>
            <div class="controls">
                <select name="groupSettingsFieldset[setWhoCanAdd]" id="groupSettingsFieldset-setWhoCanAdd" class="">
                    <option value="ALL_MEMBERS_CAN_ADD">ALL_MEMBERS_CAN_ADD</option>
                    <option value="ALL_MANAGERS_CAN_ADD">ALL_MANAGERS_CAN_ADD</option>
                    <option value="NONE_CAN_ADD">NONE_CAN_ADD</option>
                </select></div></div>
        <div class="control-group"><label for="groupSettingsFieldset-setWhoCanJoin" class="control-label optional">setWhoCanJoin</label>
            <div class="controls">
                <select name="groupSettingsFieldset[setWhoCanJoin]" id="groupSettingsFieldset-setWhoCanJoin" class="">
                    <option value="INVITED_CAN_JOIN">INVITED_CAN_JOIN</option>
                    <option value="ANYONE_CAN_JOIN">ANYONE_CAN_JOIN</option>
                    <option value="ALL_IN_DOMAIN_CAN_JOIN">ALL_IN_DOMAIN_CAN_JOIN</option>
                    <option value="CAN_REQUEST_TO_JOIN">CAN_REQUEST_TO_JOIN</option>
                </select></div></div>
        <div class="control-group"><label for="groupSettingsFieldset-setWhoCanLeaveGroup" class="control-label optional">setWhoCanLeaveGroup</label>
            <div class="controls">
                <select name="groupSettingsFieldset[setWhoCanLeaveGroup]" id="groupSettingsFieldset-setWhoCanLeaveGroup" class="">
                    <option value="ALL_MEMBERS_CAN_LEAVE">ALL_MEMBERS_CAN_LEAVE</option>
                    <option value="ALL_MANAGERS_CAN_LEAVE">ALL_MANAGERS_CAN_LEAVE</option>
                    <option value="NONE_CAN_LEAVE">NONE_CAN_LEAVE</option>
                </select></div></div>
        <div class="control-group"><label for="groupSettingsFieldset-setWhoCanViewMembership" class="control-label optional">setWhoCanViewMembership</label>
            <div class="controls">
                <select name="groupSettingsFieldset[setWhoCanViewMembership]" id="groupSettingsFieldset-setWhoCanViewMembership" class="">
                    <option value="ALL_MEMBERS_CAN_VIEW">ALL_MEMBERS_CAN_VIEW</option>
                    <option value="ALL_IN_DOMAIN_CAN_VIEW">ALL_IN_DOMAIN_CAN_VIEW</option>
                    <option value="ALL_MANAGERS_CAN_VIEW">ALL_MANAGERS_CAN_VIEW</option>
                </select></div></div>
        <div class="control-group"><label for="groupSettingsFieldset-setWhoCanViewGroup" class="control-label optional">setWhoCanViewGroup</label>
            <div class="controls">
                <select name="groupSettingsFieldset[setWhoCanViewGroup]" id="groupSettingsFieldset-setWhoCanViewGroup" class="">
                    <option value="ALL_MEMBERS_CAN_VIEW">ALL_MEMBERS_CAN_VIEW</option>
                    <option value="ANYONE_CAN_VIEW">ANYONE_CAN_VIEW</option>
                    <option value="ALL_IN_DOMAIN_CAN_VIEW">ALL_IN_DOMAIN_CAN_VIEW</option>
                    <option value="ALL_MANAGERS_CAN_VIEW">ALL_MANAGERS_CAN_VIEW</option>
                </select></div></div>
        <div class="control-group"><label for="groupSettingsFieldset-setWhoCanInvite" class="control-label optional">setWhoCanInvite</label>
            <div class="controls">
                <select name="groupSettingsFieldset[setWhoCanInvite]" id="groupSettingsFieldset-setWhoCanInvite" class="">
                    <option value="ALL_MANAGERS_CAN_INVITE">ALL_MANAGERS_CAN_INVITE</option>
                    <option value="ALL_MEMBERS_CAN_INVITE">ALL_MEMBERS_CAN_INVITE</option>
                    <option value="NONE_CAN_INVITE">NONE_CAN_INVITE</option>
                </select></div></div>
        <div class="control-group"><label for="groupSettingsFieldset-setWhoCanPostMessage" class="control-label optional">setWhoCanPostMessage</label>
            <div class="controls">
                <select name="groupSettingsFieldset[setWhoCanPostMessage]" id="groupSettingsFieldset-setWhoCanPostMessage" class="">
                    <option value="ALL_MEMBERS_CAN_POST">ALL_MEMBERS_CAN_POST</option>
                    <option value="ANYONE_CAN_POST">ANYONE_CAN_POST</option>
                    <option value="ALL_IN_DOMAIN_CAN_POST">ALL_IN_DOMAIN_CAN_POST</option>
                    <option value="ALL_MANAGERS_CAN_POST">ALL_MANAGERS_CAN_POST</option>
                    <option value="NONE_CAN_POST">NONE_CAN_POST</option>
                </select></div></div>
        <div class="control-group"><label for="groupSettingsFieldset-setWhoCanContactOwner" class="control-label optional">setWhoCanContactOwner</label>
            <div class="controls">
                <select name="groupSettingsFieldset[setWhoCanContactOwner]" id="groupSettingsFieldset-setWhoCanContactOwner" class="">
                    <option value="ANYONE_CAN_CONTACT">ANYONE_CAN_CONTACT</option>
                    <option value="ALL_IN_DOMAIN_CAN_CONTACT">ALL_IN_DOMAIN_CAN_CONTACT</option>
                    <option value="ALL_MEMBERS_CAN_CONTACT">ALL_MEMBERS_CAN_CONTACT</option>
                    <option value="ALL_MANAGERS_CAN_CONTACT">ALL_MANAGERS_CAN_CONTACT</option>
                </select></div></div>
        <div class="control-group"><label for="groupSettingsFieldset-setIsArchived" class="control-label optional">setIsArchived</label>
            <div class="controls">
                <select name="groupSettingsFieldset[setIsArchived]" id="groupSettingsFieldset-setIsArchived" class="">
                    <option value="true">true</option>
                    <option value="false">false</option>
                </select></div></div>
        <div class="control-group"><label for="groupSettingsFieldset-setAllowExternalMembers" class="control-label optional">setAllowExternalMembers</label>
            <div class="controls">
                <select name="groupSettingsFieldset[setAllowExternalMembers]" id="groupSettingsFieldset-setAllowExternalMembers" class="">
                    <option value="false">false</option>
                    <option value="true">true</option>
                </select></div></div>
        <div class="control-group"><label for="groupSettingsFieldset-setShowInGroupDirectory" class="control-label optional">setShowInGroupDirectory</label>
            <div class="controls">
                <select name="groupSettingsFieldset[setShowInGroupDirectory]" id="groupSettingsFieldset-setShowInGroupDirectory" class="">
                    <option value="false">false</option>
                    <option value="true">true</option>
                </select></div></div>
        <div class="control-group"><label for="groupSettingsFieldset-setIncludeInGlobalAddressList" class="control-label optional">setIncludeInGlobalAddressList</label>
            <div class="controls">
                <select name="groupSettingsFieldset[setIncludeInGlobalAddressList]" id="groupSettingsFieldset-setIncludeInGlobalAddressList" class="">
                    <option value="false">false</option>
                    <option value="true">true</option>
                </select></div></div>
        <div class="form-actions">
            <input type="submit" name="submit" id="submit" value="Submit" buttons="danger info primary success warning inverse link" class="btn btn-primary">
            <button name="cancel" id="cancel" type="button" value="
Cancel" buttons="danger info primary success warning inverse link" class="btn">
                Cancel</button></div></form>
    </div>





@endsection()