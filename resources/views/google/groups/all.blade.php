@extends('layouts.wrapper', [
    'pageTitle' => 'Google Users'
])

@section('content')
    {!! Breadcrumbs::render('google_groups_all') !!}

    <div class="d-flex justify-content-between align-items-center">
        <div>
            <h1>All Google Groups</h1>
            <p>This page lists every Google Group in the domain</p>
        </div>
    </div>

    @if(!empty($groups))
        @component('components.table')
            @slot('th')
                <th scope="col">@sortablelink('google_id','ID')</th>
                <th scope="col">@sortablelink('email','Email')</th>
                <th scope="col">@sortablelink('name', 'Name')</th>
                <th scope="col">@sortablelink('members_count', 'Members')</th>
                <th scope="col">Actions</th>
            @endslot
            @slot('tbody')
                @foreach ($groups as $thisGroup)
                    <tr>
                        <td>{{ $thisGroup->id }}</td>
                        <td>{{ $thisGroup->email }}</td>
                        <td>{{ $thisGroup->name}}</td>
                        <td>{{ $thisGroup->members_count }}</td>
                        <td class="d-flex flex-column flex-md-row">
                            <a href="{{ route('google.groups.show', $thisGroup) }}" class="btn btn-sm btn-primary mr-2 mb-2 mb-md-0"><i class="fas fa-list"></i> Details</a>

                        </td>
                    </tr>
                @endforeach
            @endslot
        @endcomponent
    @else
        <p>No groups returned.</p>
    @endif
@endsection()
