@extends('layouts.wrapper', [
    'pageTitle' => 'Group Presets | Edit'
])

@section('content')
    <h1>Edit Preset: {{ $preset->name }}</h1>
    {!! Form::route('google.groups.presets.update', ['preset' => $preset->id])->method('patch')->open() !!}
        {!! Form::text('name', 'Name', $preset->name)->required()->help('A name to help you recognize this Preset') !!}
        <h2>Settings:</h2>
        <div class="row">
            @foreach ($settings as $setting)
                <div class="col-3">{!! Form::select('settings[' . $setting->key . ']', $setting->key, array_merge(['' => 'NOT SET'], $setting->valuesAsOptions), $preset->getSettingValue($setting->key))->required(false) !!}</div>
            @endforeach
        </div>
        {!! Form::submit('<i class="fas fa-check"></i> Submit')->color('success') !!}
    {!! Form::close() !!}
@endsection
