@extends('layouts.wrapper', [
    'pageTitle' => 'Group Presets | Index'
])

@section('content')
    <div class="d-flex justify-content-between align-items-center">
        <h1>Group Presets</h1>
        <a href="{{ route('google.groups.presets.create') }}" class="btn btn-success">
            <i class="fas fa-plus"></i> Create New
        </a>
    </div>
    <p><strong>Group Presets</strong> are collections of settings that can be applied when creating new groups in this application. They can be customized to suit business needs. Available options are based on non-deprecated API options (complete list here: <a href="https://developers.google.com/admin-sdk/groups-settings/v1/reference/groups#resource" target="_blank">https://developers.google.com/admin-sdk/groups-settings/v1/reference/groups#resource</a>)</p>
    @if ($presets->isNotEmpty())
        @component('components.table')
            @slot('th')
                <th>ID</th>
                <th>Created By</th>
                <th>Name</th>
                <th># of Settings</th>
                <th>Created At</th>
                <th>Actions</th>
            @endslot

            @slot('tbody')
                @foreach ($presets as $preset)
                    <tr>
                        <td>{{ $preset->id }}</td>
                        <td>{{ $preset->created_by->email }}</td>
                        <td>{{ $preset->name }}</td>
                        <td>
                            {{ $preset->settings->count() }}
                            @component('components.modal', [
                                'title' => $preset->name . ': Settings',
                                'id' => 'preset_' . $preset->id . '_settings',
                            ])
                                @slot('content')
                                    <ul>
                                        @foreach ($preset->settings as $setting)
                                            <li>{{ $setting->key }} - {{ $setting->pivot->value }}</li>
                                        @endforeach
                                    </ul>
                                @endslot
                            @endcomponent
                            <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#preset_{{ $preset->id }}_settings"> <i class="fas fa-eye"></i> Show</button>
                        </td>
                        <td>{{ $preset->created_at }}</td>
                        <td class="d-flex">
                            <a href="{{ route('google.groups.presets.edit', $preset->id) }}" class="btn btn-warning btn-sm mr-2">
                                <i class="fas fa-edit"></i> Edit
                            </a>
                            {!! Form::route('google.groups.presets.destroy', ['preset' => $preset->id])->method('delete')->open() !!}
                            <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure? This cannot be undone.');">
                                <i class="fas fa-trash"></i> Delete
                            </button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @endslot
        @endcomponent
    @endif
@endsection
