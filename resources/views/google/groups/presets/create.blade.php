@extends('layouts.wrapper', [
    'pageTitle' => 'Group Presets | Create'
])

@section('content')
    <h1>Create Group Preset</h1>
    {!! Form::route('google.groups.presets.store')->method('post')->open() !!}
        {!! Form::text('name', 'Name')->required()->help('A name to help you recognize this Preset') !!}
        <h2>Settings:</h2>
        <div class="row">
            @foreach ($settings as $setting)
                <div class="col-3">{!! Form::select('settings[' . $setting->key . ']', $setting->key, array_merge(['' => 'NOT SET'], $setting->valuesAsOptions))->required(false) !!}</div>
            @endforeach
        </div>
        {!! Form::submit('<i class="fas fa-check"></i> Submit')->color('success') !!}
    {!! Form::close() !!}
@endsection
