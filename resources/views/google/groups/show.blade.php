@extends('layouts.wrapper', [
    'pageTitle' => 'Google Group Detail'
])

@section('content')
    {!! Breadcrumbs::render('google_groups_show', $group) !!}

    <h1>Google Group Details</h1>
    @if(!empty($groupSettings))
    <div class="card mb-3">

        <h5 class="card-header">Group Information</h5>
        <div class="card-body">
        <h2>{{ $groupSettings->name }} // {{ $groupSettings->email }}</h2>

        <p><strong>Description:</strong>
            @if(!empty($groupSettings->description)) {{ $groupSettings->description }} @else <em>no description provided</em>@endif</p>
        <h4>Settings:</h4>
            <ul>
                <li>Who Can Moderate Members: {{ $groupSettings->whoCanModerateMembers }} </li>
                <li>Who Can Join: {{ $groupSettings->whoCanJoin }} </li>

                <li>Who Can Leave: {{ $groupSettings->whoCanLeaveGroup }} </li>
                <li>Who Can View Memberships: {{ $groupSettings->whoCanViewMembership }} </li>
                <li>Who Can View Group: {{ $groupSettings->whoCanViewGroup }} </li>
                <li>Who Can Invite: {{ $groupSettings->whoCanInvite }} </li>
                <li>Who Can Post Messages: {{ $groupSettings->whoCanPostMessage }} </li>
                <li>Is Archived?  @if ($groupSettings->isArchived == "true") YES  // TRUE @else NO // FALSE @endif </li>
                <li>Allow Web Postings?  @if ($groupSettings->allowWebPosting == "true") YES  // TRUE @else NO // FALSE @endif </li>
                <li>Allow External Members?  @if ($groupSettings->allowExternalMembers == "true") YES // TRUE @else NO // FALSE @endif </li>
                <li>Who Can Discover Group: {{ $groupSettings->whoCanDiscoverGroup }}</li>
                <li>Include in Global Address List?  @if ($groupSettings->includeInGlobalAddressList == "true") YES // TRUE @else NO // FALSE @endif </li>
                <li>Collaborative Inbox?  @if ($groupSettings->enableCollaborativeInbox == "true") YES // TRUE @else NO // FALSE @endif </li>

            </ul>
                <a href="{{ route('google.groups.edit', $group) }}" class="btn btn-warning"><i class="fa fa-edit"></i> Update</a>
                <a href="{{ $groupWebUrl }}" class="btn btn btn-primary mr-2 mb-2 mb-md-0"><i class="fas fa-mouse-pointer"></i> View Group on Web</a>


    @else
        <p>No Group Information/Settings returned.</p>
    @endif



        </div>
    </div>
    <div class="card mb-3">

        <h5 class="card-header">Membership</h5>
        <div class="card-body">
            <p>
                <a href="{{ route('google.groups.members.edit', $group) }}" class="btn btn-warning"><i class="fa fa-edit"></i> Update</a>
            </p>
        @if($groupMembers->isNotEmpty())
            @include('partials.groups.member-table')
        @else
            <p>No Members.</p>
        @endif
        </div>
    </div>





@endsection()
