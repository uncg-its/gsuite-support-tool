@extends('layouts.wrapper', [
    'pageTitle' => 'Google Group: Create New'
])

@section('content')
    {!! Breadcrumbs::render('google_groups_create') !!}
    <h1>Google Group :: Create New</h1>
    <form method="POST" action="{{ route('google.groups.store') }}" enctype="application/x-www-form-urlencoded">
        {{ csrf_field() }}



    <div class="card mb-3">

            <h5 class="card-header">Group Information</h5>
            <div class="card-body">


                <div class="form-group row">
                    <label for="groupName" class="col-sm-3 col-form-label">Group Name <span class="badge badge-danger">Required</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="groupName" id="groupName" value="" required="required" class="form-control" aria-describedby="groupNameHelpBlock">
                        <small id="groupNameHelpBlock" class="form-text text-muted">Succinct display name of the Google Group.</small>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="groupDescription" class="col-sm-3 col-form-label">Group Description <span class="badge badge-danger">Required</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="groupDescription" id="groupDescription" value="" required="required" class="form-control">
                        <small id="groupDescriptionHelpBlock" class="form-text text-muted">Description for this Google Group</small>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="groupEmail" class="col-sm-3 col-form-label">Group Email Prefix <span class="badge badge-danger">Required</span></label>
                    <div class="col-sm-9">
                        <div class="input-group">
                        <input type="text" name="groupEmail" id="groupEmail" value="" append="@uncg.edu" required="required" class="form-control" aria-describedby="groupEmailHelpBlock">
                            <div class="input-group-append">
                                <span class="input-group-text">{{ '@'. config('google-api.domain') }}</span>
                            </div>
                        </div>
                        <small id="groupEmailHelpBlock" class="form-text text-muted">Group Email Prefix for this Google Group</small>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="ownerEmail" class="col-sm-3 col-form-label">List of owner email addresses:</label>
                    <div class="col-sm-9">
                        <textarea name="ownerEmail" id="ownerEmail" style="width: 50%; height: 150px;" class="" rows="24" cols="80"  aria-describedby="ownerEmailHelpBlock"></textarea>
                        <small id="ownerEmailHelpBlock" class="form-text text-muted">Enter each email address on a line by itself (no quotes, commas or spaces)</small>
                    </div>
                </div>

                {!! Form::select('preset', 'Group Preset', $presetsList)->required(false)->help('Choose a Preset from this list, or continue with defining settings ad-hoc. <a href="' . route('google.groups.presets.index') . '" target="_blank">See Group Presets</a>') !!}


                <div id="adHocSettings">
                    <div class="form-group row">
                        <label for="groupSettingsFieldset-setWhoCanModerateMembers" class="col-sm-3 col-form-label">setWhoCanModerateMembers</label>
                        <div class="col-sm-9">
                            <select name="groupSettingsFieldset[setWhoCanModerateMembers]" id="groupSettingsFieldset-setWhoCanModerateMembers" class="">
                                <option value="ALL_MEMBERS">ALL_MEMBERS</option>
                                <option value="OWNERS_AND_MANAGERS">OWNERS_AND_MANAGERS</option>
                                <option value="OWNERS_ONLY">OWNERS_ONLY</option>
                                <option value="NONE">NONE</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanJoin" class="col-sm-3 col-form-label">setWhoCanJoin</label>
                        <div class="col-sm-9">
                            <select name="groupSettingsFieldset[setWhoCanJoin]" id="groupSettingsFieldset-setWhoCanJoin" class="">
                                <option value="INVITED_CAN_JOIN">INVITED_CAN_JOIN</option>
                                <option value="ANYONE_CAN_JOIN">ANYONE_CAN_JOIN</option>
                                <option value="ALL_IN_DOMAIN_CAN_JOIN">ALL_IN_DOMAIN_CAN_JOIN</option>
                                <option value="CAN_REQUEST_TO_JOIN">CAN_REQUEST_TO_JOIN</option>
                            </select></div></div>
                    <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanLeaveGroup" class="col-sm-3 col-form-label">setWhoCanLeaveGroup</label>
                        <div class="col-sm-9">
                            <select name="groupSettingsFieldset[setWhoCanLeaveGroup]" id="groupSettingsFieldset-setWhoCanLeaveGroup" class="">
                                <option value="ALL_MEMBERS_CAN_LEAVE">ALL_MEMBERS_CAN_LEAVE</option>
                                <option value="ALL_MANAGERS_CAN_LEAVE">ALL_MANAGERS_CAN_LEAVE</option>
                                <option value="NONE_CAN_LEAVE">NONE_CAN_LEAVE</option>
                            </select></div></div>
                    <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanViewMembership" class="col-sm-3 col-form-label">setWhoCanViewMembership</label>
                        <div class="col-sm-9">
                            <select name="groupSettingsFieldset[setWhoCanViewMembership]" id="groupSettingsFieldset-setWhoCanViewMembership" class="">
                                <option value="ALL_MEMBERS_CAN_VIEW">ALL_MEMBERS_CAN_VIEW</option>
                                <option value="ALL_IN_DOMAIN_CAN_VIEW">ALL_IN_DOMAIN_CAN_VIEW</option>
                                <option value="ALL_MANAGERS_CAN_VIEW">ALL_MANAGERS_CAN_VIEW</option>
                            </select></div></div>
                    <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanViewGroup" class="col-sm-3 col-form-label">setWhoCanViewGroup</label>
                        <div class="col-sm-9">
                            <select name="groupSettingsFieldset[setWhoCanViewGroup]" id="groupSettingsFieldset-setWhoCanViewGroup" class="">
                                <option value="ALL_MEMBERS_CAN_VIEW">ALL_MEMBERS_CAN_VIEW</option>
                                <option value="ANYONE_CAN_VIEW">ANYONE_CAN_VIEW</option>
                                <option value="ALL_IN_DOMAIN_CAN_VIEW">ALL_IN_DOMAIN_CAN_VIEW</option>
                                <option value="ALL_MANAGERS_CAN_VIEW">ALL_MANAGERS_CAN_VIEW</option>
                            </select></div></div>
                    <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanPostMessage" class="col-sm-3 col-form-label">setWhoCanPostMessage</label>
                        <div class="col-sm-9">
                            <select name="groupSettingsFieldset[setWhoCanPostMessage]" id="groupSettingsFieldset-setWhoCanPostMessage" class="">
                                <option value="ALL_MEMBERS_CAN_POST">ALL_MEMBERS_CAN_POST</option>
                                <option value="ANYONE_CAN_POST">ANYONE_CAN_POST</option>
                                <option value="ALL_IN_DOMAIN_CAN_POST">ALL_IN_DOMAIN_CAN_POST</option>
                                <option value="ALL_MANAGERS_CAN_POST">ALL_MANAGERS_CAN_POST</option>
                                <option value="NONE_CAN_POST">NONE_CAN_POST</option>
                            </select></div></div>
                    <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanContactOwner" class="col-sm-3 col-form-label">setWhoCanContactOwner</label>
                        <div class="col-sm-9">
                            <select name="groupSettingsFieldset[setWhoCanContactOwner]" id="groupSettingsFieldset-setWhoCanContactOwner" class="">
                                <option value="ANYONE_CAN_CONTACT">ANYONE_CAN_CONTACT</option>
                                <option value="ALL_IN_DOMAIN_CAN_CONTACT">ALL_IN_DOMAIN_CAN_CONTACT</option>
                                <option value="ALL_MEMBERS_CAN_CONTACT">ALL_MEMBERS_CAN_CONTACT</option>
                                <option value="ALL_MANAGERS_CAN_CONTACT">ALL_MANAGERS_CAN_CONTACT</option>
                            </select></div></div>
                    <div class="form-group row"><label for="groupSettingsFieldset-setIsArchived" class="col-sm-3 col-form-label">setIsArchived</label>
                        <div class="col-sm-9">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setIsArchived]" id="groupSettingsFieldset-setIsArchived-true" value="true"   checked="checked" aria-describedby="groupSettingsFieldset-setIsArchived-HelpBlock">
                                <label class="form-check-label" for="groupSettingsFieldset-setIsArchived-true">True</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setIsArchived]" id="groupSettingsFieldset-setIsArchived-false" value="false" aria-describedby="groupSettingsFieldset-setIsArchived-HelpBlock">
                                <label class="form-check-label" for="groupSettingsFieldset-setIsArchived-false">False</label>
                            </div>
                            <small id="groupSettingsFieldset-setIsArchived-HelpBlock" class="form-text text-muted">Controls if group messages are archived. If disabled, previously received messages will remain archived but new messages will not be available on the web. Disabling archiving will also disable digest email mode</small>
                        </div></div>
                    <div class="form-group row"><label for="groupSettingsFieldset-setAllowExternalMembers" class="col-sm-3 col-form-label">setAllowExternalMembers</label>
                        <div class="col-sm-9">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setAllowExternalMembers]" id="groupSettingsFieldset-setAllowExternalMembers-true" value="true" aria-describedby="groupSettingsFieldset-setAllowExternalMembers-HelpBlock">
                                <label class="form-check-label" for="groupSettingsFieldset-setAllowExternalMembers-true">True</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setAllowExternalMembers]" id="groupSettingsFieldset-setAllowExternalMembers-false" value="false"  checked="checked" aria-describedby="groupSettingsFieldset-setAllowExternalMembers-HelpBlock">
                                <label class="form-check-label" for="groupSettingsFieldset-setAllowExternalMembers-false">False</label>
                            </div>
                            <small id="groupSettingsFieldset-setAllowExternalMembers-HelpBlock" class="form-text text-muted">May this group contain members not of the domain {{ '@'. config('google-api.domain') }}?</small>
                        </div>
                    </div>
                    <div class="form-group row"><label for="groupSettingsFieldset-setWhoCanDiscoverGroup" class="col-sm-3 col-form-label">setWhoCanDiscoverGroup</label>
                        <div class="col-sm-9">
                            <select name="groupSettingsFieldset[setWhoCanDiscoverGroup]" id="groupSettingsFieldset-setWhoCanDiscoverGroup" class="">
                                <option value="ANYONE_CAN_DISCOVER">ANYONE_CAN_DISCOVER</option>
                                <option value="ALL_IN_DOMAIN_CAN_DISCOVER">ALL_IN_DOMAIN_CAN_DISCOVER</option>
                                <option value="ALL_MEMBERS_CAN_DISCOVER">ALL_MEMBERS_CAN_DISCOVER</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="groupSettingsFieldset-setIncludeInGlobalAddressList" class="col-sm-3 col-form-label">setIncludeInGlobalAddressList</label>
                        <div class="col-sm-9">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setIncludeInGlobalAddressList]" id="groupSettingsFieldset-setIncludeInGlobalAddressList-true" value="true">
                                <label class="form-check-label" for="groupSettingsFieldset-setIncludeInGlobalAddressList-true">True</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setIncludeInGlobalAddressList]" id="groupSettingsFieldset-setIncludeInGlobalAddressList-false" value="false" checked="checked">
                                <label class="form-check-label" for="groupSettingsFieldset-setIncludeInGlobalAddressList-false">False</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="groupSettingsFieldset-setEnableCollaborativeInbox" class="col-sm-3 col-form-label">setEnableCollaborativeInbox</label>
                        <div class="col-sm-9">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setEnableCollaborativeInbox]" id="groupSettingsFieldset-setEnableCollaborativeInbox-true" value="true">
                                <label class="form-check-label" for="groupSettingsFieldset-setEnableCollaborativeInbox-true">True</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="groupSettingsFieldset[setEnableCollaborativeInbox]" id="groupSettingsFieldset-setEnableCollaborativeInbox-false" value="false" checked="checked">
                                <label class="form-check-label" for="groupSettingsFieldset-setEnableCollaborativeInbox-false">False</label>
                            </div>
                            <small id="groupSettingsFieldset-setEnableCollaborativeInbox-HelpBlock" class="form-text text-muted">Is this group a Collaborative Inbox?</small>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Add</button>
                            <a href="{{ route('google.groups.index') }}" class="btn btn-danger"><i class="fas fa-times"></i> Cancel</a>
                        </div>
                    </div>
                </div>
    </form>


            </div>
        </div>





@endsection

@section('scripts')
    <script>
        $('#preset').on('change', function() {
            var value = $(this).val();
            if (value === '') {
                $('#adHocSettings').show();
                return;
            }
            $('#adHocSettings').hide();
        })
    </script>
@endsection
