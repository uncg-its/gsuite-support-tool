@extends('layouts.wrapper', [
    'pageTitle' => 'Google Users'
])

@section('content')
    {!! Breadcrumbs::render('google_groups') !!}

    <div class="d-flex justify-content-between align-items-center">
        <div>
            <h1>Google Groups</h1>
        </div>
        <div class="d-flex justify-content-end align-items-center">
            <a href="{{ route('google.groups.create') }}" class="btn btn-sm btn-success"><i class="fas fa-plus"></i> Add New</a>
            <a href="{{ route('google.groups.all') }}" class="ml-2 btn btn-sm btn-warning"><i class="fas fa-list"></i> Show All</a>
            <a href="{{ route('google.groups.presets.index') }}" class="ml-2 btn btn-sm btn-dark"><i class="fas fa-cogs"></i> Presets</a>
            <a href="{{ route('google.groups.sync') }}" class="ml-2 btn btn-sm btn-info"><i class="fas fa-sync"></i> Sync from Google</a>
        </div>
    </div>
    <p>This list shows the local Group cache, which is synced regularly. You can manually sync it using the button at the upper-right.</p>

    <div class="row mb-3">
        <div class="col">
            <div class="card">
                <div class="card-header">Search / Filter</div>
                <div class="card-body">
                    {!! Form::open()->inlineForm()->get()->route('google.groups.index')->fill(collect(request()->except('_token'))) !!}
                    {!! Form::text('email', 'Email')->placeholder('Email') !!}
                    {!! Form::text('name', 'Name')->placeholder('Name') !!}
                    {!! Form::submit('<i class="fas fa-search"></i> Search')->size('sm')->color('success') !!}
                    {!! Form::anchor('<i class="fas fa-undo"></i> Reset', route('google.groups.index'))->size('sm')->color('danger') !!}
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>

    @if (request()->anyFilled(['name', 'email']))
        <div class="alert alert-warning">
            <i class="fas fa-exclamation-circle"></i> Filtering on
        </div>
    @endif

    @if(!empty($groups))

        @component('components.paginated-table', ['collection' => $groups, 'appends' => [
            'name' => request('name', ''),
            'email' => request('email', '')
        ]])
            @slot('table')
                @component('components.table')
                    @slot('th')
                        <th scope="col">@sortablelink('google_id','ID')</th>
                        <th scope="col">@sortablelink('email','Email')</th>
                        <th scope="col">@sortablelink('name', 'Name')</th>
                        <th scope="col">@sortablelink('members_count', 'Members')</th>
                        <th scope="col">Actions</th>
                    @endslot
                    @slot('tbody')
                        @foreach ($groups as $thisGroup)
                            <tr>
                                <td>{{ $thisGroup->id }}</td>
                                <td>{{ $thisGroup->email }}</td>
                                <td>{{ $thisGroup->name}}</td>
                                <td>{{ $thisGroup->members_count }}</td>
                                <td class="d-flex flex-column flex-md-row">
                                    <a href="{{ route('google.groups.show', $thisGroup) }}" class="btn btn-sm btn-primary mr-2 mb-2 mb-md-0"><i class="fas fa-list"></i> Details</a>

                                </td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent
            @endslot
        @endcomponent

    @else
        <p>No groups returned.</p>
    @endif
@endsection()
