@extends('layouts.wrapper', [
    'pageTitle' => 'Google Group Membership Edit'
])

@section('content')
    {!! Breadcrumbs::render('google_groups_show', $group) !!}

    <h1>Google Group Membership Edit</h1>
    @if(!empty($group))
        <div class="card mb-3">

            <h5 class="card-header">Group Information</h5>
            <div class="card-body">

        <h2>{{ $group->name }} // {{ $group->email }}</h2>

        <form method="POST" action="{{ route('google.groups.members.update', $group) }}" enctype="application/x-www-form-urlencoded">
            {{ csrf_field() }}

            <div class="form-group row">
                <label for="ownerEmail" class="col-sm-2 col-form-label">List of email addresses (or usernames if using default domain):</label>
                <div class="col-sm-10">
                    <textarea name="emailList" id="emailList" style="width: 50%; height: 150px;" class="" rows="24" cols="80"  aria-describedby="emailListHelpBlock"></textarea>
                    <small id="emaiListHelpBlock" class="form-text text-muted">Enter each email address on a line by itself (no quotes, commas or spaces)</small>
                </div>
            </div>

            <div class="form-group row">
                <label for="groupRole" class="col-sm-2 col-form-label">Group Role</label>
                <div class="col-sm-10">

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="groupRole" id="groupRole-Owner" value="OWNER">
                            <label class="form-check-label" for="groupRole-Owner">OWNER</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="groupRole" id="groupRole-Member" value="MEMBER"   checked="checked">
                            <label class="form-check-label" for="groupRole-Member">MEMBER</label>
                        </div>


                </div>
            </div>

            <div class="form-group row">
                <label for="groupRole" class="col-sm-2 col-form-label">Action Type</label>
                <div class="col-sm-10">

                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="action" id="action-add" value="ADD" checked="checked">
                        <label class="form-check-label" for="action-add">Add these Users</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="action" id="action-delete" value="DELETE">
                        <label class="form-check-label" for="action-delete">Delete these Users</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="action" id="action-replace" value="REPLACE"   >
                        <label class="form-check-label" for="action-replace">Replace current membership with these Users</label>
                    </div>


                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Submit</button>
                        <input type="hidden" name="groupId" id="groupId" value="{{ $group->id }}">
                        <a href="{{ route('google.groups.show', $group) }}" class="btn btn-danger"><i class="fas fa-times"></i> Cancel</a>
                    </div>
                </div>
            </div>
        </form>

        <div class="card mb-3">

        <h5 class="card-header">Membership</h5>
        <div class="card-body">
        @if($groupMembers->isNotEmpty())
            @include('partials.groups.member-table')
        @else
            <p>No Members.</p>
        @endif

        @else
        <p>Error: No Group ID.</p>
        @endif

        </div>
    </div>

@endsection()
