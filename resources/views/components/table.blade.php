<table class="table table-sm table-hover">
    <thead class="thead-light">
    <tr>
        {{ $th }}
    </tr>
    </thead>
    <tbody>
    {{ $tbody }}
    </tbody>
</table>