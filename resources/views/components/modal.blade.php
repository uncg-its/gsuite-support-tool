<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="{{ \Str::camel($title) }}" id="{{ $id }}"
     aria-hidden="true">
    <div class="modal-dialog modal-{{ $size ?? 'lg' }}">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="{{ \Str::camel($title) }}">{{ $title }}</h5>
            </div>
            <div class="modal-body">
                {{ $content }}
            </div>
        </div>
    </div>
</div>
