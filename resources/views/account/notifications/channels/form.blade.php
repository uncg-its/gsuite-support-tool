{!! Form::select('type', 'Type', $types) !!}
{!! Form::text('name', 'Nickname')->help('Name to help you recognize this channel') !!}
{!! Form::text('key', 'Value')->help('The email / phone / webhook URL') !!}
<div class="carrier-wrapper d-none">
    {!! Form::select('carrier', 'SMS Carrier', $carriers)->help('SMS Carrier') !!}
</div>

{!! Form::submit('Submit')->color('success')->size('sm') !!}