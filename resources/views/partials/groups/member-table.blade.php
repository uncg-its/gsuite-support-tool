@component('components.paginated-table', ['collection' => $groupMembers])
    @slot('table')
        @component('components.table')
            @slot('th')
                <th scope="col">@sortablelink('id','ID')</th>
                <th scope="col">@sortablelink('email','Email')</th>
                <th scope="col">@sortablelink('type', 'Type')</th>
                <th scope="col">@sortablelink('role', 'Role')</th>
                <th scope="col">@sortablelink('status', 'Status')</th>
                <th scope="col">Actions</th>
            @endslot
            @slot('tbody')
                @foreach ($groupMembers as $thisMember)
                    <tr>
                        <td>{{ $thisMember->id }}</td>
                        <td>{{ $thisMember->email }}</td>
                        <td>{{ $thisMember->type}}</td>
                        <td>@if ($thisMember->role=="OWNER") <span class="badge badge-info">{{ $thisMember->role}}</span> @else {{ $thisMember->role}} @endif</td>
                        <td>{{ $thisMember->status }}</td>
                        <td class="d-flex flex-column flex-md-row">
                            <a href="{{ route('google.users.key', ['key' => $thisMember->id]) }}" class="btn btn-sm btn-primary mr-2 mb-2 mb-md-0"><i class="fas fa-list"></i> Details</a>
                        </td>
                    </tr>
                @endforeach
            @endslot
        @endcomponent
    @endslot
@endcomponent
