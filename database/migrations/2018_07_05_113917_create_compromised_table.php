<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompromisedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compromised', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('email',255);
            $table->timestamp('processed')->useCurrent();
            $table->timestamp('compromised')->nullable($value=TRUE);
            $table->boolean('verified')->default(FALSE);
            $table->boolean('incident_status')->default(FALSE);
            $table->string('incident_details',255)->nullable($value=TRUE);
            $table->boolean('was_on_monitored_list')->default(FALSE);
            $table->string('two_factor_status',255);
            $table->text('destination_services')->nullable($value=TRUE);
            $table->string('user_type',255)->nullable($value=TRUE);
            $table->ipAddress('ip_address')->nullable($value=TRUE);
            $table->string('geo-location',255)->nullable($value=TRUE);
            $table->string('compromise_type',255)->nullable($value=TRUE);
            $table->boolean('token_processed')->default(FALSE);
            $table->text('token_details')->nullable($value=TRUE);
            $table->boolean('forward_rule_processed')->default(FALSE);
            $table->text('forward_rule_details')->nullable($value=TRUE);
            $table->boolean('google_password_processed')->default(FALSE);
            $table->text('google_password_details')->nullable($value=TRUE);
            $table->boolean('csam_processed')->default(FALSE);
            $table->text('csam_details')->nullable($value=TRUE);
            $table->boolean('google_suspend_processed')->default(FALSE);
            $table->text('google_suspend_details')->nullable($value=TRUE);
            $table->boolean('google_unsuspend_processed')->default(FALSE);
            $table->text('google_unsuspend_details')->nullable($value=TRUE);
            $table->string('user_who_processed',255)->nullable($value=TRUE);
            $table->text('notes')->nullable($value=TRUE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compromised');
    }
}