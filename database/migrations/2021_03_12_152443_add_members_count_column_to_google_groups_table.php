<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMembersCountColumnToGoogleGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('google_groups', function (Blueprint $table) {
            $table->unsignedInteger('members_count')->after('etag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('google_groups', function (Blueprint $table) {
            $table->dropColumn('members_count');
        });
    }
}
