<?php

use App\GroupSetting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAndSeedGroupSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_settings', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->json('values');
        });

        $settings = [
            [
                'key'    => 'allowExternalMembers',
                'values' => ['true','false']
            ],
            [
                'key'    => 'allowGoogleCommunication',
                'values' => ['true', 'false']
            ],
            [
                'key'    => 'allowWebPosting',
                'values' => ['true', 'false']
            ],
            [
                'key'    => 'archiveOnly',
                'values' => ['true', 'false']
            ],
            [
                'key'    => 'customRolesEnabledForSettingsToBeMerged',
                'values' => ['true', 'false']
            ],
            [
                'key'    => 'enableCollaborativeInbox',
                'values' => ['true', 'false']
            ],
            [
                'key'    => 'favoriteRepliesOnTop',
                'values' => ['true', 'false']
            ],
            [
                'key'    => 'includeCustomFooter',
                'values' => ['true', 'false']
            ],
            [
                'key'    => 'includeInGlobalAddressList',
                'values' => ['true', 'false']
            ],
            [
                'key'    => 'isArchived',
                'values' => ['true', 'false']
            ],
            [
                'key'    => 'membersCanPostAsTheGroup',
                'values' => ['true', 'false']
            ],
            [
                'key'    => 'messageModerationLevel',
                'values' => ['MODERATE_NONE','MODERATE_ALL_MESSAGES','MODERATE_NON_MEMBERS','MODERATE_NEW_MEMBERS']
            ],
            [
                'key'    => 'replyTo',
                'values' => ['REPLY_TO_CUSTOM','REPLY_TO_SENDER','REPLY_TO_LIST','REPLY_TO_OWNER','REPLY_TO_IGNORE','REPLY_TO_MANAGERS']
            ],
            [
                'key'    => 'sendMessageDenyNotification',
                'values' => ['true', 'false']
            ],
            [
                'key'    => 'spamModerationLevel',
                'values' => ['ALLOW','MODERATE','SILENTLY_MODERATE','REJECT']
            ],
            [
                'key'    => 'whoCanAssistContent',
                'values' => ['ALL_MEMBERS','OWNERS_AND_MANAGERS','MANAGERS_ONLY','OWNERS_ONLY','NONE']
            ],
            [
                'key'    => 'whoCanContactOwner',
                'values' => ['ALL_IN_DOMAIN_CAN_CONTACT','ALL_MANAGERS_CAN_CONTACT','ALL_MEMBERS_CAN_CONTACT','ANYONE_CAN_CONTACT']
            ],
            [
                'key'    => 'whoCanDiscoverGroup',
                'values' => ['ANYONE_CAN_DISCOVER','ALL_IN_DOMAIN_CAN_DISCOVER','ALL_MEMBERS_CAN_DISCOVER']
            ],
            [
                'key'    => 'whoCanJoin',
                'values' => ['ANYONE_CAN_JOIN','ALL_IN_DOMAIN_CAN_JOIN','INVITED_CAN_JOIN','CAN_REQUEST_TO_JOIN']
            ],
            [
                'key'    => 'whoCanLeaveGroup',
                'values' => ['ALL_MANAGERS_CAN_LEAVE','ALL_MEMBERS_CAN_LEAVE','NONE_CAN_LEAVE']
            ],
            [
                'key'    => 'whoCanModerateContent',
                'values' => ['ALL_MEMBERS','OWNERS_AND_MANAGERS','OWNERS_ONLY','NONE']
            ],
            [
                'key'    => 'whoCanModerateMembers',
                'values' => ['ALL_MEMBERS','OWNERS_AND_MANAGERS','OWNERS_ONLY','NONE']
            ],
            [
                'key'    => 'whoCanPostMessage',
                'values' => ['NONE_CAN_POST','ALL_MANAGERS_CAN_POST','ALL_MEMBERS_CAN_POST','ALL_OWNERS_CAN_POST','ALL_IN_DOMAIN_CAN_POST','ANYONE_CAN_POST']
            ],
            [
                'key'    => 'whoCanViewGroup',
                'values' => ['ANYONE_CAN_VIEW','ALL_IN_DOMAIN_CAN_VIEW','ALL_MEMBERS_CAN_VIEW','ALL_MANAGERS_CAN_VIEW','ALL_OWNERS_CAN_VIEW']
            ],
            [
                'key'    => 'whoCanViewMembership',
                'values' => ['ALL_IN_DOMAIN_CAN_VIEW','ALL_MEMBERS_CAN_VIEW','ALL_MANAGERS_CAN_VIEW']
            ],
        ];

        collect($settings)->each(fn ($setting) => GroupSetting::create($setting));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_settings');
    }
}
