<?php

use App\CcpsCore\Role;
use App\CcpsCore\Permission;
use Illuminate\Database\Migrations\Migration;

class OverhaulAcl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            Role::whereIn('name', ['edit.google', 'viewer.google'])->delete();
            $roles = [
                [
                    'source_package' => 'app',
                    'name'           => 'gsuite.admin',
                    'display_name'   => 'G Suite - Admin',
                    'description'    => 'Administrator of the G Suite service'
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'gsuite.support',
                    'display_name'   => 'G Suite - Support',
                    'description'    => 'Support Staff for the G Suite service'
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'service-desk',
                    'display_name'   => 'Service Desk',
                    'description'    => 'Service Desk employees'
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'info-security',
                    'display_name'   => 'Information Security',
                    'description'    => 'Member of Information Security group'
                ],
            ];
            foreach ($roles as $role) {
                Role::create($role);
            }

            Permission::whereIn('name', ['google-api.edit', 'google-api.view'])->delete();
            $permissions = [
                [
                    'source_package' => 'app',
                    'name'           => 'gsuite.users.view',
                    'display_name'   => 'G Suite Users - View',
                    'description'    => 'Can view G Suite Users via API tools',
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'gsuite.users.create',
                    'display_name'   => 'G Suite Users - Create',
                    'description'    => 'Can create G Suite Users via API tools',
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'gsuite.users.update',
                    'display_name'   => 'G Suite Users - Update',
                    'description'    => 'Can edit/update G Suite Users via API tools',
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'gsuite.users.delete',
                    'display_name'   => 'G Suite Users - Delete',
                    'description'    => 'Can delete/suspend G Suite Users via API tools',
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'gsuite.groups.view',
                    'display_name'   => 'G Suite Groups - View',
                    'description'    => 'Can view G Suite Groups via API tools',
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'gsuite.groups.create',
                    'display_name'   => 'G Suite Groups - Create',
                    'description'    => 'Can create G Suite Groups via API tools',
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'gsuite.groups.update',
                    'display_name'   => 'G Suite Groups - Update',
                    'description'    => 'Can edit/update G Suite Groups via API tools',
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'gsuite.groups.delete',
                    'display_name'   => 'G Suite Groups - Delete',
                    'description'    => 'Can delete/suspend G Suite Groups via API tools',
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'gsuite.whatis.view',
                    'display_name'   => 'What Is Tool',
                    'description'    => 'Can use the What Is tool',
                ],
            ];
            foreach ($permissions as $permission) {
                Permission::create($permission);
            }

            $map = [
                'admin' => [
                    'gsuite.users.view',
                    'gsuite.users.create',
                    'gsuite.users.update',
                    'gsuite.users.delete',
                    'gsuite.groups.view',
                    'gsuite.groups.create',
                    'gsuite.groups.update',
                    'gsuite.groups.delete',
                    'gsuite.whatis.view',
                ],
                'gsuite.admin' => [
                    'gsuite.users.view',
                    'gsuite.users.create',
                    'gsuite.users.update',
                    'gsuite.users.delete',
                    'gsuite.groups.view',
                    'gsuite.groups.create',
                    'gsuite.groups.update',
                    'gsuite.groups.delete',
                    'gsuite.whatis.view',
                ],
                'gsuite.support' => [
                    'gsuite.users.view',
                    'gsuite.groups.view',
                    'gsuite.whatis.view',
                ],
                'service-desk' => [
                    'gsuite.users.view',
                    'gsuite.groups.view',
                    'gsuite.whatis.view',
                ],
                'info-security' => [
                    'gsuite.users.view',
                    'gsuite.groups.view',
                    'gsuite.whatis.view',
                ],
            ];

            $allRoles = Role::get();
            $allPermissions = Permission::get();

            foreach ($map as $role => $permissions) {
                $allRoles->where('name', $role)->first()->permissions()->attach($allPermissions->whereIn('name', $permissions)->pluck('id'));
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // nope.
    }
}
