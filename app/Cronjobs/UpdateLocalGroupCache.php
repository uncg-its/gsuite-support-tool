<?php

namespace App\Cronjobs;

use Uncgits\Ccps\Models\Cronjob;
use App\Services\GoogleGroupService;
use Uncgits\Ccps\Helpers\CronjobResult;

class UpdateLocalGroupCache extends Cronjob
{
    protected $schedule = '0 0,8,12,16,20 * * *'; // default schedule (overridable in database)
    protected $display_name = 'Update Local Group Cache'; // default display name (overridable in database)
    protected $description = 'Updates the local cache of Google Groups for easier searching / parsing'; // default description name (overridable in database)
    protected $tags = []; // default tags (overridable in database)

    protected function execute()
    {
        $service = new GoogleGroupService;
        $results = $service->updateLocalGroupCache();

        \Log::channel('cache')->info('Google Group cache updated', [
            'category'  => 'cache',
            'operation' => 'update',
            'result'    => 'success',
            'data'      => [
                'results' => $results
            ]
        ]);

        return new CronjobResult(true);
    }
}
