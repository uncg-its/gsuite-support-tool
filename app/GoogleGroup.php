<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class GoogleGroup extends Model
{
    use HasFactory, Sortable;

    public $sortable = [
        'google_id',
        'name',
        'email',
        'members_count'
    ];

    protected $primaryKey = 'google_id';
    public $incrementing = false;

    protected $guarded = [];

    protected $casts = [
        'group_object'    => 'object',
        'settings_object' => 'object',
    ];

    // accessores

    public function getIdAttribute()
    {
        return $this->google_id;
    }

    // other

    public function getMembers()
    {
        return collect(\GoogleApi::listGroupMembers($this->email));
    }
}
