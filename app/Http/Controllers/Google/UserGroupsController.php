<?php

namespace App\Http\Controllers\Google;

use App\Http\Controllers\Controller as BaseController;

use Illuminate\Http\Request;
use App\GoogleApi;
use Google_Service_Exception;

class UserGroupsController extends BaseController
{
    /*
    |----------------------------------------------
    | Constructor
    |----------------------------------------------
    */

    public function __construct()
    {
        $this->googleApi = new GoogleApi();
        $this->middleware('permission:gsuite.*')->only(['index']);
        $this->middleware('permission:gsuite.users.view')->only(['getUsers', 'getAdmins', 'showUser']);
        $this->middleware('permission:gsuite.users.delete')->only(['suspendUser']);
    }

    public function showUserGroups(Request $request)
    {
        // Validation stuff
        if (isset($request->key) && !empty($request->key)) {
            try {
                $userInfo = $this->googleApi->getUser($request->key);
                $userPhoto = $this->googleApi->getUserPhoto($request->key, true);
                $userAliases = $this->googleApi->listUserAliases($request->key);
                $userTokens = $this->googleApi->listUserTokens($request->key);
                $userGroups = collect($this->googleApi->listGroups(['key' => $request->key]));

                //$userLogins = $this->googleApi->listUserLogins(['key' => $request->key]);
                //$userLogins = $this->googleApi->activityList(['userKey' => $request->key,'applicationName'=>'login','optParams'=>['maxResults'=>10]]);
                return view('google.users.groups', compact('userInfo', 'userPhoto', 'userAliases', 'userTokens', 'userGroups', 'request'));
            } catch (Google_Service_Exception $e) {
                flash('User not found in domain - this user may be external.');
                return view('google.users.groups')->with([
                  'userInfo' => ''
              ]);
            }
        }
    }
}
