<?php

namespace App\Http\Controllers\Google;

use App\Http\Controllers\Controller as BaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\GoogleApi;
use App\GoogleGroup;
use App\GroupPreset;
use App\Services\GoogleGroupService;
use Uncgits\Ccps\Support\CcpsPaginator;
use Google_Service_Exception;

class GroupController extends BaseController
{

  /*
    |----------------------------------------------
    | Constructor
    |----------------------------------------------
    */

    public function __construct()
    {
        $this->googleApi = new GoogleApi();
        $this->middleware('permission:gsuite.groups.view')->only(['index', 'show']);
        $this->middleware('permission:gsuite.groups.create')->only(['create', 'store']);
        $this->middleware('permission:gsuite.groups.update')->only(['edit', 'update', 'editMembers', 'updateMembers']);
    }

    public function all()
    {
        return view('google.groups.all')->with([
            'groups' => GoogleGroup::sortable('email')->get()
        ]);
    }

    public function index(Request $request)
    {
        $query = GoogleGroup::sortable(['email' => 'asc']); // default

        if ($request->filled('name')) {
            $query->where('name', 'like', '%' . $request->name . '%');
        }

        if ($request->filled('email')) {
            $query->where('email', 'like', '%' . $request->email . '%');
        }

        return view('google.groups.index')->with([
            'groups' => $query->paginate()
        ]);
    }

    public function show(GoogleGroup $group, Request $request)
    {
        $parts = explode('@', $group->email);
        $emailComponents = [
            'local'  => $parts[0],
            'domain' => $parts[1]
        ];

        $groupWebUrl = "https://groups.google.com/a/" . $emailComponents['domain'] . "/g/" . $emailComponents['local'];

        return view('google.groups.show')->with([
            'group'         => $group,
            'groupSettings' => $group->settings_object,
            'groupMembers'  => new CcpsPaginator($group->getMembers()),
            'groupWebUrl'   => $groupWebUrl
        ]);
    }


    public function create()
    {
        $presetsList = GroupPreset::get()->mapWithKeys(function ($preset) {
            return [$preset->id => $preset->name];
        })->prepend('NO PRESET', '')->toArray();
        return view('google.groups.create')->with([
            'presetsList' => $presetsList
        ]);
    }

    public function store(Request $request)
    {
        $theNewGroupEmail = $request->groupEmail . '@'. config('google-api.domain');

        $theNewGroup = [
            'email'       => $theNewGroupEmail,
            'description' => $request->groupDescription,
            'name'        => $request->groupName
        ];
        // Explode the Owners
        $theNewGroupOwners = explode("\n", $request->ownerEmail);
        if ($request->filled('preset')) {
            // settings should be loaded from the preset
            $theNewGroupSettings = GroupPreset::findOrFail($request->preset)->settings->mapWithKeys(function ($setting) {
                $methodName = 'set' . \Str::studly($setting->key);
                return [$methodName => $setting->pivot->value];
            })->toArray();
        } else {
            $theNewGroupSettings = $request->groupSettingsFieldset;
        }

        // FIRST, Create the Group
        $created = false;
        try {
            $created = $this->googleApi->insertGroup($theNewGroup);
        } catch (Google_Service_Exception $e) {
            $theCode = $e->getCode();
            flash('Google Group ' .$request->groupName . ' ('.$theNewGroupEmail.') could not be created. Error: '. $theCode, 'danger');
        }

        sleep(1);

        // SECOND, Loop through the owners (trimming) and insert (aka create)
        try {
            foreach ($theNewGroupOwners as $k => $memberEmail) {
                $this->googleApi->insertGroupMember($theNewGroupEmail, trim($memberEmail), 'OWNER');
            }
        } catch (Google_Service_Exception $e) {
            $theCode = $e->getCode();
            flash('Error inserting members for Google Group ' .$request->groupName . ' ('.$theNewGroupEmail.'). Error: '. $theCode, 'danger');
        }

        sleep(1);
        //dd($theNewGroupSettings);

        // THIRD, Update the Group's settings, from the default to the desired
        try {
            $this->googleApi->patchGroupSettings($theNewGroupEmail, $theNewGroupSettings);
        } catch (Google_Service_Exception $e) {
            $theCode = $e->getCode();
            flash('Error assigning Group Settings for Google Group ' .$request->groupName . ' ('.$theNewGroupEmail.'). Error: '. $theCode, 'danger');
        }

        if ($created) {
            $service = new GoogleGroupService;
            $local = $service->updateLocalGroup($created);
            flash('Google Group created successfully: ' .$request->groupName . ' ('.$theNewGroupEmail.')', 'success');
            return redirect()->route('google.groups.show', $local);
        }

        return redirect()->route('google.groups.index');
    }

    public function edit(GoogleGroup $group, Request $request)
    {
        $groupSettingsPossibleValues = $this->googleApi->getGoogleGroupSettingsPossibleValues();

        return view('google.groups.edit')->with([
            'group'                       => $group,
            'groupSettings'               => $group->settings_object,
            'groupSettingsPossibleValues' => $groupSettingsPossibleValues,
        ]);
    }

    public function update(GoogleGroup $group, Request $request)
    {
        $theGroupEmail = $request->groupEmail . '@'. config('google-api.domain');

        $editedGroupInfo = [
            'email'       => $theGroupEmail,
            'description' => $request->groupDescription,
            'name'        => $request->groupName
        ];

        $editedGroupSettings = $request->groupSettingsFieldset;
        //dd($editedGroupSettings);
        $service = new GoogleGroupService;
        try {
            $updated = $this->googleApi->updateGroup($group->google_id, $editedGroupInfo);
        } catch (Google_Service_Exception $e) {
            $theCode = $e->getCode();
            flash('Google Group '.$group->google_id.' could not be updated. Error: '. $theCode, 'danger');
        }

        sleep(1);
        // patch the Group's settings, from the default to the desired
        try {
            $this->googleApi->patchGroupSettings($theGroupEmail, $editedGroupSettings);
        } catch (Google_Service_Exception $e) {
            $theCode = $e->getCode();
            flash('Google Group '.$group->google_id.' could not be edited. Error: '. $theCode, 'danger');
        }

        sleep(1);

        // update locally
        $local = $service->updateLocalGroup($updated);

        // SUCCESS!
        flash('Google Group edited successfully: ' .$request->groupName . ' ('.$theGroupEmail.')', 'success');
        return redirect()->route('google.groups.show', $group);
    }

    public function sync()
    {
        $service = new GoogleGroupService;
        $service->updateLocalGroupCache();
        flash('Local group cache updated')->success();
        return redirect()->route('google.groups.index');
    }

    public function editMembers(GoogleGroup $group, Request $request)
    {
        $members = $group->getMembers();

        return view('google.groups.members')->with([
            'group'        => $group,
            'groupMembers' => new CcpsPaginator($members),
        ]);
    }

    public function updateMembers(GoogleGroup $group, Request $request)
    {
        $groupEmails = collect(explode("\n", $request->emailList))->map(function ($email) {
            $email = trim($email);
            if (strpos($email, '@') === false) {
                $email = $email . '@' . config('google-api.domain');
            }
            return $email;
        });

        $groupRole = $request->groupRole;
        $action = $request->action;

        if ($action === 'REPLACE') {
            // get current membership
            $members = collect(\GoogleApi::listGroupMembers($request->groupId))->where('role', $groupRole)->pluck('email');
            // deletes: current minus overlapping members
            $deletes = $members->diff($groupEmails);
            // adds: submitted list minus original current
            $adds = collect($groupEmails)->diff($members);

            foreach ($deletes as $email) {
                try {
                    $this->googleApi->deleteGroupMember($group->email, $email, $groupRole);
                } catch (Google_Service_Exception $e) {
                    flash('Error removing ' . $email . ': ' . $e->getCode() . ' - ' . $e->getErrors()[0]['message']);
                }
            }
            foreach ($adds as $email) {
                try {
                    $this->googleApi->insertGroupMember($group->email, $email, $groupRole);
                } catch (Google_Service_Exception $e) {
                    flash('Error adding ' . $email . ': ' . $e->getCode() . ' - ' . $e->getErrors()[0]['message']);
                }
            }
            flash('Google Group membership edited successfully: ' .$group->name . ' ('.$group->email.')')->success();
        } else {
            foreach ($groupEmails as $k => $thisEmail) {
                try {
                    if ($action === 'ADD') {
                        $this->googleApi->insertGroupMember($group->email, $thisEmail, $groupRole);
                    } elseif ($action === 'DELETE') {
                        $this->googleApi->deleteGroupMember($group->email, $thisEmail, $groupRole);
                    }
                } catch (Google_Service_Exception $e) {
                    flash('Could not ' . strtolower($action) . ' user ' . $thisEmail . ': '. $e->getCode() . ' - ' . $e->getErrors()[0]['message'])->error();
                }
            }
            flash('Google Group membership edited successfully: ' .$group->name . ' ('.$group->email.')')->success();
        }
        return redirect()->route('google.groups.show', $group);
    }

    public function getAllCalendars(Request $request)
    {
        $theSubject = '';

        if (isset($request->calendarId) && !empty($request->calendarId)) {
            // by default, uses the id of the service account
            $theSubject = $request->calendarId;
        } else {
            $theSubject = auth()->user()->username;
        }


        $factory = new GoogleClientFactory($theSubject);

        $client = $factory->getGoogleClient();

        //  NEED TO WORK ON CATCHING ERRORS
        try {
            $calendar_service = new \Google_Service_Calendar($client);
        } catch (Google_Service_Exception $e) {
            $theCode = $e->getCode();
            flash('Error:' . $theCode, 'danger');
        }

        $calendarList = $calendar_service->calendarList->listCalendarList();

        // An array of Calendars
        $calendarListItems = $calendarList->getItems();
        //dd($calendarList->getItems());

        // Summary of the first Calendar in the array
        //dd($calendarListItems[0]->getId());

        // $calendarId = "krmmccla2@uncg.net";
        // $calendarList = $client->events->listEvents($calendarId);
        // dd($calendarList);


        // }
        return view('google.calendars', compact('calendarListItems', 'request', 'theSubject'));
    }
}
