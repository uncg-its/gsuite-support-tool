<?php

namespace App\Http\Controllers\Google;

use App\Http\Controllers\Controller as BaseController;

use Illuminate\Http\Request;
use App\GoogleApi;
use Google_Service_Exception;

class UserEmailController extends BaseController
{

    /*
   |----------------------------------------------
   | Constructor
   |----------------------------------------------
   */

    public function __construct()
    {
        $this->googleApi = new GoogleApi();
        $this->middleware('permission:gsuite.*')->only(['index']);
        $this->middleware('permission:gsuite.users.view')->only(['getUsers', 'getAdmins', 'showUser']);
        $this->middleware('permission:gsuite.users.delete')->only(['suspendUser']);
    }



    public function showUserEmail(Request $request)
    {
        // Validation stuff
        if (isset($request->key) && !empty($request->key)) {
            try {
                $userInfo = $this->googleApi->getUser($request->key);
                $userPhoto = $this->googleApi->getUserPhoto($request->key, true);
                $userAliases = $this->googleApi->listUserAliases($request->key);
                $emailForwards = $this->googleApi->listEmailForwards($request->key);
                $emailFilters = $this->googleApi->listEmailFilters($request->key)->filter ?? [];
                $emailDelegates = $this->googleApi->listEmailDelegates($request->key);
                $emailLabels = $this->googleApi->listEmailLabels($request->key);
                $emailVacation = $this->googleApi->getEmailVacation($request->key);
                $emailSendAs = $this->googleApi->getEmailSendAs($request->key);
                $emailLanguage = $this->googleApi->getEmailLanguage($request->key);
                $emailPop = $this->googleApi->getEmailPop($request->key);
                $emailImap = $this->googleApi->getEmailImap($request->key);
                $emailProfile = $this->googleApi->getEmailProfile($request->key);

                return view('google.users.email', compact(
                    'userInfo',
                    'userPhoto',
                    'userAliases',
                    'emailForwards',
                    'emailFilters',
                    'emailDelegates',
                    'emailLabels',
                    'emailVacation',
                    'emailSendAs',
                    'emailLanguage',
                    'emailPop',
                    'emailImap',
                    'emailProfile',
                    'request'
                ));
            } catch (Google_Service_Exception $e) {
                flash('User not found in domain - this user may be external.');
                return view('google.users.show')->with([
                'userInfo' => ''
            ]);
            }
        }
    }
}
