<?php

namespace App\Http\Controllers\Google;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Csv\Reader;

class BulkActionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:gsuite.users.update')->only(['rename', 'confirmRename', 'performRename']);
        $this->middleware('permission:gsuite.users.delete')->only(['delete', 'confirmDelete', 'performDelete']);
    }

    public function rename()
    {
        return view('google.users.bulk.rename');
    }

    public function confirmRename(Request $request)
    {
        $request->validate([
            'csv'    => 'required|file',
            'prefix' => 'nullable|required_without:suffix',
            'suffix' => 'nullable|required_without:prefix',
        ]);

        // provide a list of operations for the user
        $filename = \Str::random(15) . '.csv';
        $request->file('csv')->storeAs('', $filename, 'temp');
        $csv = Reader::createFromPath(\Storage::disk('temp')->path($filename), 'r');
        try {
            // do we have a username header?
            $csv->setHeaderOffset(0);
            $headerRow = $csv->getHeader();
            $header = null;
            foreach ($headerRow as $headerName) {
                if (strtolower($headerName) === 'username') {
                    $header = $headerName;
                    continue;
                }
            }
            if (is_null($header)) {
                throw new \Exception('Username not found in header.');
            }

            $newNames = [];
            foreach ($csv->getRecords() as $record) {
                $newNames[$record[$headerName]] = $request->get('prefix', '') . $record[$headerName] . $request->get('suffix', '');
            }

            return view('google.users.bulk.rename-confirm')->with([
                'newNames' => $newNames
            ]);
        } catch (\Throwable $e) {
            flash('Error: ' . $e->getMessage())->danger();
            return redirect()->back();
        } finally {
            unlink(\Storage::disk('temp')->path($filename)); // executes even after the return statement
        }
    }

    public function performRename(Request $request)
    {
        $domain = config('google-api.domain');
        [$successful, $failed] = collect($request->names)->partition(function ($newName, $oldName) use ($domain) {
            try {
                \GoogleApi::updateUser($oldName . '@' . $domain, [
                    'primaryEmail' => $newName . '@' . $domain
                ]);
                return true;
            } catch (\Throwable $e) {
                return false;
            }
        });

        if ($successful->isNotEmpty()) {
            flash('Successful updates: ' . $successful->count())->success();
        }
        if ($failed->isNotEmpty()) {
            flash('Failed updates: ' . $failed->count() . ' (' . $failed->keys()->implode(',') . ')')->error();
        }
        \Log::channel('bulk')->info('Bulk rename performed', [
            'category'  => 'bulk',
            'operation' => 'rename',
            'result'    => 'success',
            'data'      => [
                'successful' => $successful->implode(','),
                'failed'     => $failed->implode(','),
                'user'       => \Auth::user()->email
            ]
        ]);
        return redirect()->route('google.users.bulk-rename');
    }

    public function delete()
    {
        return view('google.users.bulk.delete');
    }

    public function confirmDelete(Request $request)
    {
        $request->validate([
            'csv' => 'required|file',
        ]);

        // provide a list of operations for the user
        $filename = \Str::random(15) . '.csv';
        $request->file('csv')->storeAs('', $filename, 'temp');
        $csv = Reader::createFromPath(\Storage::disk('temp')->path($filename), 'r');
        try {
            // do we have a username header?
            $csv->setHeaderOffset(0);
            $headerRow = $csv->getHeader();
            $header = null;
            foreach ($headerRow as $headerName) {
                if (strtolower($headerName) === 'username') {
                    $header = $headerName;
                    continue;
                }
            }
            if (is_null($header)) {
                throw new \Exception('Username not found in header.');
            }

            $toDelete = [];
            foreach ($csv->getRecords() as $record) {
                $toDelete[] = $record[$headerName];
            }

            return view('google.users.bulk.delete-confirm')->with([
                'toDelete' => $toDelete
            ]);
        } catch (\Throwable $e) {
            flash('Error: ' . $e->getMessage())->error();
            return redirect()->back();
        } finally {
            unlink(\Storage::disk('temp')->path($filename)); // executes even after the return statement
        }
    }

    public function performDelete(Request $request)
    {
        $domain = config('google-api.domain');
        [$successful, $failed] = collect($request->names)->partition(function ($username) use ($domain) {
            try {
                \GoogleApi::deleteUser($username . '@' . $domain);
                return true;
            } catch (\Throwable $e) {
                return false;
            }
        });

        if ($successful->isNotEmpty()) {
            flash('Successful deletions: ' . $successful->count())->success();
        }
        if ($failed->isNotEmpty()) {
            flash('Failed deletions: ' . $failed->count() . ' (' . $failed->implode(',') . ')')->error();
        }

        \Log::channel('bulk')->info('Bulk deletion performed', [
            'category'  => 'bulk',
            'operation' => 'delete',
            'result'    => 'success',
            'data'      => [
                'successful' => $successful->implode(','),
                'failed'     => $failed->implode(','),
                'user'       => \Auth::user()->email
            ]
        ]);
        return redirect()->route('google.users.bulk-delete');
    }
}
