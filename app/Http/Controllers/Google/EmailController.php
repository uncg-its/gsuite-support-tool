<?php

namespace App\Http\Controllers\Google;

use App\Http\Controllers\Controller as BaseController;

use Illuminate\Http\Request;
use App\GoogleApi;
use Google_Service_Exception;

class EmailController extends BaseController
{
  
    /*
   |----------------------------------------------
   | Constructor
   |----------------------------------------------
   */

    public function __construct()
    {
        $this->googleApi = new GoogleApi();
        $this->middleware('permission:gsuite.*')->only(['index']);
        $this->middleware('permission:gsuite.users.view')->only(['getUsers', 'getAdmins', 'showUser']);
        $this->middleware('permission:gsuite.users.delete')->only(['suspendUser']);
    }



    public function showEmailConfig(Request $request)
    {
        // Validation stuff
        if (isset($request->key) && !empty($request->key)) {
            try {
                $userInfo = $this->googleApi->getUser($request->key);
                $userPhoto = $this->googleApi->getUserPhoto($request->key, true);
                $userAliases = $this->googleApi->listUserAliases($request->key);
                $emailForwards = $this->googleApi->listEmailForwards($request->key);
                $emailFilters = $this->googleApi->listEmailFilters($request->key);
                $emailDelegates = $this->googleApi->listEmailDelegates($request->key);
                $emailLabels = $this->googleApi->listEmailLabels($request->key);

                return view('google.users.email', compact('userInfo', 'userPhoto', 'userAliases', 'emailForwards', 'emailFilters', 'emailDelegates', 'emailLabels', 'request'));
            } catch (Google_Service_Exception $e) {
                flash('User not found in domain - this user may be external.');
                return view('google.users.show')->with([
                'userInfo' => ''
            ]);
            }
        }
    }
}
