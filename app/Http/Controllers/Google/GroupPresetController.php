<?php

namespace App\Http\Controllers\Google;

use App\GroupPreset;
use App\GroupSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class GroupPresetController extends Controller
{
    public function index()
    {
        return view('google.groups.presets.index')->with([
            'presets' => GroupPreset::with('settings')->get()
        ]);
    }

    public function create()
    {
        return view('google.groups.presets.create')->with([
            'settings' => GroupSetting::get()
        ]);
    }

    public function store(Request $request)
    {
        $settingRules = GroupSetting::get()->mapWithKeys(function ($setting) {
            return ['settings.' . $setting->key => 'nullable|in:' . implode(',', $setting->values)];
        })->toArray();

        $validated = $request->validate(array_merge([
            'name' => 'required|unique:group_presets,name',
        ], $settingRules));

        $settings = collect($validated['settings'])->reject(fn ($item) => $item === null)->toArray();

        \DB::transaction(function () use ($validated, $settings) {
            $preset = GroupPreset::create([
                'name'          => $validated['name'],
                'created_by_id' => \Auth::user()->id,
            ]);
            $settingsToAttach = GroupSetting::whereIn('key', array_keys($settings))->get();
            foreach ($settingsToAttach as $setting) {
                $preset->settings()->attach($setting->id, ['value' => $settings[$setting->key]]);
            }
        });

        flash('Preset created!')->success();
        return redirect()->route('google.groups.presets.index');
    }

    public function edit(GroupPreset $preset)
    {
        return view('google.groups.presets.edit')->with([
            'preset'   => $preset,
            'settings' => GroupSetting::get()
        ]);
    }
    public function update(GroupPreset $preset, Request $request)
    {
        $settingRules = GroupSetting::get()->mapWithKeys(function ($setting) {
            return ['settings.' . $setting->key => 'nullable|in:' . implode(',', $setting->values)];
        })->toArray();

        $validated = $request->validate(array_merge([
            'name' => ['required', Rule::unique('group_presets', 'name')->ignore($preset)],
        ], $settingRules));

        $settings = collect($validated['settings'])->reject(fn ($item) => $item === null)->toArray();

        \DB::transaction(function () use ($preset, $validated, $settings) {
            $preset->update([
                'name' => $validated['name'],
            ]);

            $preset->settings()->sync([]);
            $settingsToAttach = GroupSetting::whereIn('key', array_keys($settings))->get();
            foreach ($settingsToAttach as $setting) {
                $preset->settings()->attach($setting->id, ['value' => $settings[$setting->key]]);
            }
        });

        flash('Preset updated!')->success();
        return redirect()->route('google.groups.presets.index');
    }

    public function destroy(GroupPreset $preset)
    {
        $preset->settings()->sync([]);
        $preset->delete();
        flash('Preset deleted!')->success();
        return redirect()->route('google.groups.presets.index');
    }
}
