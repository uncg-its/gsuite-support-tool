<?php

namespace App\Http\Controllers\Google;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WhatisController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:gsuite.whatis.view']);
    }

    public function index()
    {
        return view('google.whatis.index');
    }

    public function search(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);

        // is it a user?
        $query = $request->email . ' orgUnitPath=/';

        $thing = null;
        $thingPrefix = 'a';
        $thingUrl = null;

        $result = \GoogleApi::listUsers(['query' => $query]);
        if (count($result['users']) > 0) {
            $thing = $result['users'][0];
            $isAlias = in_array($request->email, $thing->aliases ?? []);
            if ($isAlias) {
                $thingType = 'alias';
                $thingSubtype = null;
                $thingPrefix = 'an';
            } else {
                $thingType = 'user';
                $thingSubtype = strpos($thing->orgUnitPath, 'secondary') === false ? 'primary' : 'secondary';
            }
            $thingUrl = route('google.users.show', ['key' => $thing->id]);
        } else {
            // if not, is it a group?
            try {
                $result = \GoogleApi::getGroup($request->email);
                $thing = $result;

                // get settings
                $settings = \GoogleApi::getGroupSettings($request->email);
                $thingType = 'group';
                $thingSubtype = $settings->enableCollaborativeInbox === 'true' ? 'collaborative inbox' : 'standard';

                $thing->settings = $settings;
                $thingUrl = route('google.groups.show', $thing->id);
            } catch (\Google_Service_Exception $e) {
                //
            }
        }

        if (is_null($thing)) {
            return response()->json(['errors' => 'Not found in users or groups'], 404);
        }

        $thing->type = $thingType;
        $thing->subtype = $thingSubtype;
        $thing->prefix = $thingPrefix;
        $thing->gstUrl = $thingUrl;

        $thing = (array) $thing;

        // remove those '\u0000' characters. unnecessary overhead?

        // foreach ($thing as $key => $value) {
        //     $newKey = preg_replace('/[\x00-\x1F\x7F]/', '', $key);
        //     if ($newKey !== $key) {
        //         $thing[$newKey] = $value;
        //         unset($thing[$key]);
        //         dump($newKey);
        //     }
        // }

        return response()->json(['thing' => $thing]);
    }
}
