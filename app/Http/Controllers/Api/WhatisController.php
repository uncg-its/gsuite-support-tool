<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WhatisController extends Controller
{
    public function get(Request $request, $query)
    {
        // Making sure the value has the domain
        $domain = config('google-api.domain');

        if (strpos($query, $domain) === false) {
            $query = $query . '@' . $domain;
        }

        $email = $query;
        // is it a user?
        $query = $query . ' orgUnitPath=/';

        $thing = null;
        $thingPrefix = 'a';
        $thingUrl = null;

        $result = \GoogleApi::listUsers(['query' => $query]);
        if (count($result['users']) > 0) {
            $thing = $result['users'][0];
            $isAlias = in_array($email, $thing->aliases ?? []);
            if ($isAlias) {
                $thingType = 'alias';
                $thingSubtype = null;
                $thingPrefix = 'an';
            } else {
                $thingType = 'user';
                $thingSubtype = strpos($thing->orgUnitPath, 'secondary') === false ? 'primary' : 'secondary';
            }
            $thingUrl = route('google.users.show', ['key' => $thing->id]);
        } else {
            // if not, is it a group?
            try {
                $result = \GoogleApi::getGroup($email);
                $thing = $result;

                // get settings
                $settings = \GoogleApi::getGroupSettings($email);
                $thingType = 'group';
                $thingSubtype = $settings->enableCollaborativeInbox === 'true' ? 'collaborative inbox' : 'standard';

                $thing->settings = $settings;
                $thingUrl = route('google.groups.show', $thing->id);
            } catch (\Google_Service_Exception $e) {
                //
            }
        }

        if (is_null($thing)) {
            return response()->json(['errors' => 'Not found in users or groups'], 404);
        }

        $thing->type = $thingType;
        $thing->subtype = $thingSubtype;
        $thing->prefix = $thingPrefix;
        $thing->gstUrl = $thingUrl;

        $thing = (array)$thing;

        return response()->json(['thing' => $thing]);
    }
}
