<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Google_Service_Exception;

class UserController extends Controller
{
    private function parseUsername($username)
    {
        if (strpos($username, '@') !== false) {
            return $username;
        }
        return $username . '@' . config('google-api.domain');
    }

    public function get($username)
    {
        try {
            $userInfo = \GoogleApi::getUser($this->parseUsername($username));
            return response()->json(['query' => $username, 'user' => $userInfo], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function aliases($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $aliases = \GoogleApi::listUserAliases($username);
            return response()->json(['query' => $username, 'aliases' => $aliases], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function tokens($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $tokens = \GoogleApi::listUserTokens($username);
            return response()->json(['query' => $username, 'tokens' => $tokens], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function groups($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $groups = \GoogleApi::listGroups(['key' => $username]);
            return response()->json(['query' => $username, 'groups' => $groups], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function emailAutoForward($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $auto = \GoogleApi::getEmailAutoForward($username);
            return response()->json(['query' => $username, 'auto' => $auto], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function emailDelegates($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $delegates = \GoogleApi::listEmailDelegates($username);
            return response()->json(['query' => $username, 'delegates' => $delegates], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function emailFilters($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $filters = \GoogleApi::listEmailFilters($username);
            return response()->json(['query' => $username, 'filters' => $filters], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function emailForwards($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $forwards = \GoogleApi::listEmailForwards($username);
            return response()->json(['query' => $username, 'forwards' => $forwards], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function emailImap($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $imap = \GoogleApi::getEmailImap($username);
            return response()->json(['query' => $username, 'imap' => $imap], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function emailLabels($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $labels = \GoogleApi::listEmailLabels($username);
            return response()->json(['query' => $username, 'labels' => $labels], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function emailLabelDetails($username, $id)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $labelId = \GoogleApi::getEmailLabel($username, $id);
            return response()->json(['query' => $username, 'labelId' => $labelId], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function emailLanguage($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $language = \GoogleApi::getEmailLanguage($username);
            return response()->json(['query' => $username, 'language' => $language], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function emailPop($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $pop = \GoogleApi::getEmailPop($username);
            return response()->json(['query' => $username, 'pop' => $pop], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function emailProfile($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $profile = \GoogleApi::getEmailProfile($username);
            return response()->json(['query' => $username, 'profile' => $profile], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function emailSendAs($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $sendAs = \GoogleApi::getEmailSendAs($username);
            return response()->json(['query' => $username, 'sendAs' => $sendAs], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function emailVacation($username)
    {
        try {
            $username = $username . '@' . config('google-api.domain');
            $vacation = \GoogleApi::getEmailVacation($username);
            return response()->json(['query' => $username, 'vacation' => $vacation], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }
}
