<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Google_Service_Exception;

class GroupController extends Controller
{
    private function parseGroupname($group)
    {
        if (strpos($group, '@') !== false) {
            return $group;
        }
        return $group . '@' . config('google-api.domain');
    }

    private function parseUsername($username)
    {
        if (strpos($username, '@') !== false) {
            return $username;
        }
        return $username . '@' . config('google-api.domain');
    }

    public function get($group)
    {
        try {
            $group = \GoogleApi::getGroup($this->parseGroupname($group));
            return response()->json(['query' => $group, 'group' => $group], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function getGroupMember($group, $username)
    {
        try {
            $member = \GoogleApi::getGroupMember($this->parseGroupname($group), $this->parseGroupname($username));
            return response()->json(['query' => $group, 'member' => $member], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function hasGroupMember($group, $username)
    {
        try {
            $member = \GoogleApi::hasGroupMember($this->parseGroupname($group), $this->parseGroupname($username));
            return response()->json(['query' => $group, 'member' => $member], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function getGroupSettings($group)
    {
        try {
            $group = \GoogleApi::getGroupSettings($this->parseGroupname($group));
            return response()->json(['query' => $group, 'group' => $group], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function listGroupMembers($group)
    {
        try {
            $members = \GoogleApi::listGroupMembers($this->parseGroupname($group));
            return response()->json(['query' => $group, 'members' => $members], 200);
        } catch (Google_Service_Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }
}
