<?php

namespace App\Http\Controllers\Auth;

use Uncgits\Ccps\Controllers\Auth\ResetPasswordController as BaseController;

class ResetPasswordController extends BaseController
{
    public function __construct() {
        parent::__construct();
    }
}
