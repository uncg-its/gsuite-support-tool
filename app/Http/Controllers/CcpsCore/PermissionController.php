<?php

namespace App\Http\Controllers\CcpsCore;

use Uncgits\Ccps\Controllers\PermissionController as BaseController;

class PermissionController extends BaseController
{
    public function __construct() {
        parent::__construct();
    }
}
