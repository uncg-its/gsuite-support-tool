<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\GoogleApi;
use Google_Service_Exception;

class GoogleController extends Controller
{


    //  protected $client;
    // protected $service;
    // protected $subject;

    // protected $googleApi;

    /*
   |----------------------------------------------
   | Constructor
   |----------------------------------------------
   */

    public function __construct()
    {
        $this->googleApi = new GoogleApi();
        $this->middleware('permission:gsuite.*')->only(['index']);
        $this->middleware('permission:gsuite.users.view')->only(['getUsers', 'getAdmins', 'showUser']);
        $this->middleware('permission:gsuite.users.delete')->only(['suspendUser']);
    }


    public function index()
    {
        return view('google.index');
    }

    public function getDomainInfo()
    {
        // $site = $this->gsuiteApi->getSiteInfo();
        $info = "testing";
        return view('google.info')->with(compact('info'));
    }


    /*
     *   USER Related Functions
     *   https://developers.google.com/admin-sdk/directory/v1/reference/users
     */

    /*
     *   Builds a query to search for users in User: List
     *   https://developers.google.com/admin-sdk/directory/v1/reference/users/list
     *   https://developers.google.com/admin-sdk/directory/v1/guides/search-users
     */

    public function getUsers(Request $request)
    {
        $theRequest['query'] = '';
        $theRequest['pageToken'] = '';

        if (isset($request->userSearch) && !empty($request->userSearch)) {
            $theRequest['query'] .= $request->userSearch;
        }

        if (isset($request->isAdmin) && !empty($request->isAdmin)) {
            $theRequest['query'] .= " isAdmin=TRUE";
        }

        if (isset($request->OU) && !empty($request->OU)) {
            $theRequest['query'] .= " orgUnitPath=$request->OU";
        }

        if (isset($request->go) && !empty($request->go)) {
            $theRequest['pageToken'] = $request->go;
        }
        // dd($theRequest);
        $response = $this->googleApi->listUsers($theRequest);
        return view('google.users.index', [
            'users'             => $response['users'],
            'nextPageToken'     => $response['nextPageToken'],
        ]);
    }

    public function getAdmins()
    {
        $theRequest['query'] = "isAdmin=TRUE";
        $users = $this->googleApi->listUsers($theRequest);
        return view('google.users.index', compact('users', 'request'));
    }

    
    public function showUser(Request $request)
    {
        // Validation stuff
        if (isset($request->key) && !empty($request->key)) {
            try {
                $userInfo = $this->googleApi->getUser($request->key);
                $userPhoto = $this->googleApi->getUserPhoto($request->key, true);
                $userAliases = $this->googleApi->listUserAliases($request->key);
                $userTokens = $this->googleApi->listUserTokens($request->key);
                $userGroups = collect($this->googleApi->listGroups(['key' => $request->key]));

                //$userLogins = $this->googleApi->listUserLogins(['key' => $request->key]);
                //$userLogins = $this->googleApi->activityList(['userKey' => $request->key,'applicationName'=>'login','optParams'=>['maxResults'=>10]]);
                return view('google.users.show', compact('userInfo', 'userPhoto', 'userAliases', 'userTokens', 'userGroups', 'request'));
            } catch (Google_Service_Exception $e) {
                flash('User not found in domain - this user may be external.');
                return view('google.users.show')->with([
                    'userInfo' => ''
                ]);
            }
        }
    }


    public function suspendUser(Request $request)
    {
        if (isset($request->suspend) && !empty($request->suspend)) {
            $userKey = $request->suspend;
            $theUser = $this->googleApi->getUser($userKey);
            $suspend_update = [
                'suspended'   => 'true',
                'orgUnitPath' => '/suspended',
                'password'    => password_hash('test', PASSWORD_DEFAULT)
            ];

            try {
                $this->googleApi->updateUser($userKey, $suspend_update);
                // event(new processedAsCompromised('processed','GSuite Account '. $request->userKey . 'processed as compromised', [ 'user'=> $user ] ));
                flash(
                    'GSuite account ' . $theUser->primaryEmail . ' (' . $theUser->name->fullName . ') proccessed successfully.',
                    'success'
                );
                return redirect()->back();
            } catch (Google_Service_Exception $e) {
                $theCode = $e->getCode();
                flash('GSuite account ' . $request->userKey . ' could not be processed. Error: ' . $theCode, 'danger');
            }
        }
    }
}
