<?php

namespace App\Http\Middleware\CcpsCore;

use Closure;
use Illuminate\Support\Facades\Auth;
use Lavary\Menu\Facade as Menu;

class DefineAppMenu
{
    private function generateModuleLinks($menu)
    {
        if ($user = Auth::user()) {
            // read modules from config and auto-generate menu
            $adminPrivs = [];
            foreach (config('ccps.modules') as $key => $module) {
                if ($user->hasPermission($module['required_permissions'])) {
                    $adminPrivs[] = $module;
                }
            }

            if (count($adminPrivs) > 0) {
                // Define Admin menu
                $menu->add('Admin', route('admin'));

                foreach ($adminPrivs as $priv => $module) {
                    if ($module['parent'] == 'admin') {
                        $menu->admin->add($module['title'], route($module['index']));
                    } else {
                        if (empty($module['parent'])) {
                            $menu->add($module['title'], route($module['index']));
                        }
                    }
                }
            }
        }
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // define application menus here

        Menu::make('nav', function ($menu) {
            // base for unauthenticated users
            $menu->add('Home');

            // for logged-in users
            if ($user = Auth::user()) {

                // Google menu
                if ($user->isAbleTo('gsuite.*')) {
                    $menu->add('Google', route('google'));
                    //    $menu->google->add('Google Info', route('google.info'));

                    if ($user->isAbleTo('gsuite.users.*')) {
                        $menu->google->add('Users', route('google.users.index'));
                    }
                    if ($user->isAbleTo('gsuite.groups.*')) {
                        $menu->google->add('Groups', route('google.groups.index'));
                    }
                    if ($user->isAbleTo('gsuite.whatis.view')) {
                        $menu->google->add('What Is', route('google.whatis.index'));
                    }
                    //    $menu->google->add('Calendars', route('google.calendars'));
                }

                // for authenticated users, generate links and Admin menu from modules
                $this->generateModuleLinks($menu);
            }
        });

        return $next($request);
    }
}
