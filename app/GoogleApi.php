<?php

namespace App;

use Uncgits\GoogleApiLaravel\GoogleApi as BaseTest;
use App\CcpsCore\DbConfig;

class GoogleApi extends BaseTest
{

    /**
     * GoogleApi constructor.
     *
     * wrapper around the parent wrapper class so that we can ensure that the debugbar is active before trying to use it.
     */
    public function __construct()
    {
        parent::__construct();

        if (DbConfig::getConfig('debugbar') != 'enabled') {
            $this->debugMode = false;
        }
    }

    public function getGoogleGroupSettingsPossibleValues()
    {
        $values = parent::getGoogleGroupSettingsPossibleValues();
        // shim
        $values['whoCanModerateMembers'] = ['ALL_MEMBERS', 'OWNERS_AND_MANAGERS', 'OWNERS_ONLY', 'NONE'];
        $values['whoCanDiscoverGroup'] = ['ANYONE_CAN_DISCOVER', 'ALL_IN_DOMAIN_CAN_DISCOVER', 'ALL_MEMBERS_CAN_DISCOVER'];

        return $values;
    }
}
