<?php

namespace App\Services;

use App\GoogleGroup;

class GoogleGroupService
{
    public function updateLocalGroupCache()
    {
        // get local
        $localGroups = GoogleGroup::select('google_id', 'etag')
            ->get()
            ->keyBy('google_id');

        // build remote
        $remoteGroups = collect(\GoogleApi::listGroups())
            ->keyBy('id');

        // compare and diff

        // adds
        $toBeAdded = $remoteGroups->diffKeys($localGroups);
        $toBeAdded->each(function ($group) {
            $this->updateLocalGroup($group);
        });

        // deletes
        $toBeDeleted = $localGroups->diffKeys($remoteGroups);
        $toBeDeleted->each(function ($group) {
            $group->delete();
        });

        // updates
        $toBeUpdated = $remoteGroups->intersectByKeys($localGroups)->filter(function ($remoteGroup, $remoteKey) use ($localGroups) {
            $localGroup = $localGroups[$remoteKey];
            return $localGroup->etag !== $remoteGroup->etag;
        });
        $toBeUpdated->each(function ($group) {
            $this->updateLocalGroup($group); // this would be remote data
        });

        return [
            'added'   => $toBeAdded->count(),
            'deleted' => $toBeDeleted->count(),
            'updated' => $toBeUpdated->count(),
        ];
    }

    public function updateLocalGroup($group)
    {
        $groupSettings = \GoogleApi::getGroupSettings($group->email);

        return GoogleGroup::updateOrCreate(
            [
                'google_id'       => $group->id,
            ],
            [
                'email'           => $group->email,
                'name'            => $group->name,
                'etag'            => $group->etag,
                'members_count'   => $group->directMembersCount ?? 0,
                'group_object'    => $group,
                'settings_object' => $groupSettings
            ]
        );
    }
}
