<?php

namespace App;

use App\CcpsCore\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class GroupPreset extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    // relationships

    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }

    public function settings()
    {
        return $this->belongsToMany(GroupSetting::class)->withPivot('value');
    }

    // other

    public function getSettingValue($key)
    {
        $setting = $this->settings()->where('key', $key)->first();
        if (is_null($setting)) {
            return '';
        }

        return $setting->pivot->value;
    }
}
