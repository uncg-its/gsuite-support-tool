<?php

namespace App\CcpsCore;

use App\GroupPreset;
use Uncgits\Ccps\Models\User as BaseModel;

class User extends BaseModel
{
    // relationships
    public function presets()
    {
        return $this->hasMany(GroupPreset::class, 'created_by_id');
    }
}
