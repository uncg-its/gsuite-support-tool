<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use Uncgits\Ccps\Exceptions\LockFileWriteException;
use Uncgits\Ccps\Helpers\Composer;

class appDeploy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:deploy {--p|prompt : Steps through each choice with yes/no prompts, allowing you to choose which to apply}';

    /**
     * Name of the signature file that we will install in upgrades/ folder after successful init (with extension)
     *
     * @var string
     */
    protected $signatureFilename = 'app-deploy.txt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deploy instance of this app';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        try {
            $this->info('Initializing App Instance ...');

            $this->checkForSignatureFile();

            // ------ fill in Mail info
            $this->setUpLdapInEnv();

            // ------ write signature file
            $this->writeSignatureFile();

            // FINAL CONFIRMATION
            $this->info('------------------------------------------');
            $this->info('APP DEPLOYMENT IS COMPLETE!');
            $this->info('------------------------------------------');

        } catch(\Exception $e) {
            $this->error('App Deployment Failed: ' . $e->getMessage());
        }

        return true;

    }

    protected function setUpLdapInEnv() {
        if (!$this->option('prompt') || $this->confirm('Set up Google variables in .env?')) {

            $fields = [
                    'GOOGLE_APPLICATION_CREDENTIALS' => ['type' => 'choice', 'values' => ['./../google-creds.json', 'enter path myself manually later']],
                    'GOOGLE_DOMAIN' => ['type' => 'choice', 'values' => ['uncg.net','uncg.edu','manually enter later']],
                    'GOOGLE_ADMIN_ACCOUNT' => ['type' => 'anticipate', 'values' => ['']],
                    'GOOGLE_ACCESS_TYPE' => ['type' => 'choice', 'values' => ['offline']],

            ];

            $this->updateEnvWithUserInput($fields);

        } else {
            $this->info('Skipped LDAP setup');
        }
    }

    protected function writeSignatureFile() {
        $installedUpgradesFolder = base_path('upgrades');
        if (!File::isDirectory($installedUpgradesFolder)) {
            $this->info('/upgrades folder does not exist; creating.');
            File::makeDirectory($installedUpgradesFolder);
        }

        // is app/upgrades folder writable?
        if (!File::isWritable($installedUpgradesFolder)) {
            throw new \Exception('app/upgrades folder is not writable! Cannot write signature file. Check permissions.');
        }

        // put timestamp data in new signature file, to be inserted into app/upgrades
        $contents = Carbon::now()->toDateTimeString();
        $bytesWritten = File::put($installedUpgradesFolder . '/' . $this->signatureFilename, $contents);
        if ($bytesWritten === false) {
            throw new Exception('Initialization completed but corresponding signature file could not be written to app/upgrades. Please check before continuing.');
        } else {
            $this->info('Initialization signature file ' . $this->signatureFilename . ' written to /upgrades successfully.');
        }
    }

    protected function checkForSignatureFile() {
        $commandName = $this->getName();
        if (File::exists(base_path('upgrades/' . $this->signatureFilename))) {
            $this->error('WARNING: It appears that you have already run the ' . $commandName . ' command!');
            if (!$this->confirm('Continue anyway?')) {
                throw new \Exception('Operation aborted');
            }
        }
    }

    protected function updateEnvWithUserInput($fields, $propertyToUpdate = null) {
        $env = file_get_contents('.env');

        foreach($fields as $field => $info) {
            $placeholder = '*' . $field . '*';
            $promptText = 'Value for ' . $field;

            if (isset($info['extraText'])) {
                $promptText .= ' ' . $info['extraText'];
            }

            if (strpos($env, $placeholder)) {
                $inputType = $info['type'];
                switch ($inputType) {
                    case 'anticipate':
                    case 'choice':
                        $value = $this->$inputType($promptText, $info['values']);
                        break;
                    case 'secret':
                    case 'ask':
                        $value = $this->$inputType($promptText);
                        break;
                    default:
                        $value = $this->ask($promptText);
                        break;
                }

                $env = str_replace($placeholder, $value, $env);

                // update local object property if we need to do so
                if (!is_null($propertyToUpdate)) {
                    $this->$propertyToUpdate[$field] = $value;
                }

                $this->info('Value set.');
            } else {
                $this->info('Placeholder value for ' . $field . ' not found - value must already be entered. Skipping.');
            }
        }

        if (file_put_contents('.env', $env)) {
            $this->info('New .env written successfully.');
        } else {
            $this->error('New .env file could not be written!');
        }
    }
}
