<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class GroupSetting extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    public $timestamps = false;

    protected $casts = [
        'values' => 'array'
    ];

    // relationships

    public function presets()
    {
        return $this->belongsToMany(GroupPreset::class);
    }

    // accessors
    public function getValuesAsOptionsAttribute()
    {
        return array_combine($this->values, $this->values);
    }
}
