<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'CcpsCore\HomeController@index')->name('home');
Route::get('home', 'CcpsCore\HomeController@index');

Route::group(['prefix' => 'account'], function () {
    Route::get('/', 'CcpsCore\AccountController@index')->name('account');
    Route::get('/settings', 'CcpsCore\AccountController@settings')->name('account.settings');
    Route::post('/settings', 'CcpsCore\AccountController@updateSettings')->name('account.settings.update');
    Route::get('/profile', 'CcpsCore\AccountController@profile')->name('profile.show');
    Route::get('/profile/edit', 'CcpsCore\AccountController@editProfile')->name('profile.edit');
    Route::patch('/profile/', 'CcpsCore\AccountController@updateProfile')->name('profile.update');

    Route::prefix('tokens')->name('account.tokens.')->group(function () {
        Route::get('/', 'CcpsCore\AccountController@tokens')->name('index');
        Route::get('create', 'CcpsCore\AccountController@createToken')->name('create');
        Route::post('/', 'CcpsCore\AccountController@storeToken')->name('store');
        Route::get('{token}', 'CcpsCore\AccountController@editToken')->name('edit');
        Route::patch('{token}', 'CcpsCore\AccountController@updateToken')->name('update');
        Route::delete('{token}', 'CcpsCore\AccountController@revokeToken')->name('revoke');
    });

    Route::group(['prefix' => 'notifications'], function () {
        Route::get('/', 'CcpsCore\AccountController@notifications')->name('account.notifications.index');
        Route::get('/channels/create', 'CcpsCore\AccountController@createChannel')->name('account.notifications.channels.create');
        Route::post('/channels/create', 'CcpsCore\AccountController@storeChannel')->name('account.notifications.channels.store');
        Route::delete('/channels/{channel}', 'CcpsCore\AccountController@destroyChannel')->name('account.notifications.channels.destroy');
        Route::get(
            '/channels/{channel}/resend-verification',
            'CcpsCore\ChannelVerificationController@resend'
        )->name('account.notifications.channels.resend-verification');
        Route::get(
            '/channels/{channel}/verify',
            'CcpsCore\ChannelVerificationController@verify'
        )->name('account.notifications.channels.verify');
        Route::get(
            '/channels/{channel}/test',
            'CcpsCore\ChannelVerificationController@test'
        )->name('account.notifications.channels.test');

        Route::get('/configure', 'CcpsCore\AccountController@configure')->name('account.notifications.configure');
        Route::post('/configure', 'CcpsCore\AccountController@configureSave')->name('account.notifications.configure.save');
    });
});

// CCPS Core routes
Uncgits\Ccps\Support\Facades\CcpsCore::routes();
// For local auth
Auth::routes();

// Socialite - User Account OAuth Routes
Route::get('auth/{provider}', 'CcpsCore\AuthController@redirectToProvider')->name('oauth');
Route::get('auth/{provider}/callback', 'CcpsCore\AuthController@handleProviderCallback')->name('oauth.callback');


// Route::get('apistats', '\Uncgits\CcpsApiLog\Http\Controllers\StatisticsController@index');


// -----------------------
// Google Viewer app routes
// -----------------------

Route::group(['prefix' => 'google', 'middleware' => ['auth']], function () {
    Route::get('/', 'GoogleController@index')->name('google');
    Route::get('/info', 'GoogleController@info')->name('google.info');
    Route::group(['prefix' => 'activity'], function () {
        Route::get('/', 'GoogleController@getActivity')->name('google.activity.index');
        Route::post('/', 'GoogleController@getActivity')->name('google.activity.index');

        // Route::get('/all', 'GoogleController@usersAll')->name('google.users.all');
        //  Route::get('/active', 'GoogleController@usersActive')->name('google.users.active');
        //  Route::get('/search', 'GoogleController@usersSearch')->name('google.users.search');
    });
    Route::group(['prefix' => 'users'], function () {
        Route::post('/suspend', 'GoogleController@suspendUser')->name('google.users.suspend');

        Route::get('/show/', 'GoogleController@showUser')->name('google.users.show');

        Route::get('bulk-rename', 'Google\BulkActionController@rename')->name('google.users.bulk-rename');
        Route::post('bulk-rename', 'Google\BulkActionController@confirmRename')->name('google.users.bulk-rename-confirm');
        Route::post('bulk-rename/confirm', 'Google\BulkActionController@performRename')->name('google.users.bulk-rename-perform');

        Route::get('bulk-delete', 'Google\BulkActionController@delete')->name('google.users.bulk-delete');
        Route::post('bulk-delete', 'Google\BulkActionController@confirmDelete')->name('google.users.bulk-delete-confirm');
        Route::post('bulk-delete/confirm', 'Google\BulkActionController@performDelete')->name('google.users.bulk-delete-perform');

        Route::get('/admins', 'GoogleController@getAdmins');
        Route::get('/{key}', 'GoogleController@showUser')->name('google.users.key');
        Route::get('/{key}/logins', 'GoogleController@showUserLogins')->name('google.users.logins');
        Route::get('/{key}/email/', 'Google\UserEmailController@showUserEmail')->name('google.users.email');
        Route::get('/{key}/groups/', 'Google\UserGroupsController@showUserGroups')->name('google.users.groups');

        Route::get('/', 'GoogleController@getUsers')->name('google.users.index');

        Route::post('/', 'GoogleController@getUsers')->name('google.users.index');


        // Route::get('/all', 'GoogleController@usersAll')->name('google.users.all');
        //  Route::get('/active', 'GoogleController@usersActive')->name('google.users.active');
        //  Route::get('/search', 'GoogleController@usersSearch')->name('google.users.search');
    });
    Route::group(['prefix' => 'groups'], function () {
        Route::get('/', 'Google\GroupController@index')->name('google.groups.index');
        Route::get('create', 'Google\GroupController@create')->name('google.groups.create');
        Route::get('sync', 'Google\GroupController@sync')->name('google.groups.sync');
        Route::get('all', 'Google\GroupController@all')->name('google.groups.all');
        Route::post('store', 'Google\GroupController@store')->name('google.groups.store');
        Route::get('{group}/edit', 'Google\GroupController@edit')->name('google.groups.edit');
        Route::post('{group}/edit', 'Google\GroupController@update')->name('google.groups.update');

        Route::get('/queue', 'Google\GroupController@getQueue')->name('google.groups.queue');

        Route::get('{group}/members', 'Google\GroupController@editMembers')->name('google.groups.members.edit');
        Route::post('{group}/members', 'Google\GroupController@updateMembers')->name('google.groups.members.update');

        // Route::get('/all', 'GoogleController@usersAll')->name('google.users.all');
        //  Route::get('/active', 'GoogleController@usersActive')->name('google.users.active');
        //  Route::get('/search', 'GoogleController@usersSearch')->name('google.users.search');
        Route::resource('presets', 'Google\GroupPresetController', ['as' => 'google.groups']);

        Route::get('{group}', 'Google\GroupController@show')->name('google.groups.show');
    });

    Route::prefix('whatis')->name('google.whatis.')->group(function () {
        Route::get('/', 'Google\WhatisController@index')->name('index');
        Route::post('/search', 'Google\WhatisController@search')->name('search');
    });

    /*
        Route::group(['prefix' => 'calendars'], function() {
            Route::get('/', 'GoogleApiController@getAllCalendars')->name('google.calendars');
            Route::post('/', 'GoogleApiController@getAllCalendars')->name('google.calendars');

        });
    */



    // Route::get('/statistics', '\Uncgits\CcpsApiLog\Http\Controllers\StatisticsController@index')->name('statistics')->middleware('permission:gsuite.view');
});

// Route::group(['prefix' => 'compromised', 'middleware' => ['auth']], function () {
//     Route::get('/', 'CompromisedController@index')->name('compromised');
//     Route::get('/process', 'CompromisedController@process')->name('compromised.process');
// });
