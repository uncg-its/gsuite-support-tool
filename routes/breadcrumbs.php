<?php

// HOME
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push("Home", route('home'));
});

// ACCOUNT
Breadcrumbs::register('account', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Account", route('account'));
});
Breadcrumbs::register('profile.show', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("My Profile", route('profile.show'));
});
Breadcrumbs::register('profile.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("Edit Profile", route('profile.edit'));
});
Breadcrumbs::register('account.settings', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("Account Settings", route('account.settings'));
});

// Notifications
Breadcrumbs::register('notifications.index', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push('Notifications', route('account.notifications.index'));
});
Breadcrumbs::register('notifications.channels.create', function ($breadcrumbs) {
    $breadcrumbs->parent('notifications.index');
    $breadcrumbs->push('Add New Channel', route('account.notifications.channels.create'));
});
Breadcrumbs::register('notifications.configure', function ($breadcrumbs) {
    $breadcrumbs->parent('notifications.index');
    $breadcrumbs->push('Configure', route('account.notifications.configure'));
});

// API Tokens
Breadcrumbs::register('tokens.index', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push('API Tokens', route('account.tokens.index'));
});
Breadcrumbs::register('tokens.create', function ($breadcrumbs) {
    $breadcrumbs->parent('tokens.index');
    $breadcrumbs->push('Create API Token', route('account.tokens.create'));
});
Breadcrumbs::register('tokens.edit', function ($breadcrumbs, $token) {
    $breadcrumbs->parent('tokens.index');
    $breadcrumbs->push('Edit API Token ' . $token->id, route('account.tokens.edit', $token));
});

// ADMIN
Breadcrumbs::register('admin', function ($breadcrumbs) {
    $breadcrumbs->push("Admin", route('admin'));
});


// GOOGLE
Breadcrumbs::register('google', function ($breadcrumbs) {
    $breadcrumbs->push("Google", route('google'));
});
Breadcrumbs::register('google_users', function ($breadcrumbs) {
    $breadcrumbs->parent('google');
    $breadcrumbs->push("Users", route('google.users.index'));
});
Breadcrumbs::register('google_users_show', function ($breadcrumbs) {
    $breadcrumbs->parent('google');
    $breadcrumbs->push("Users", route('google.users.index'));
    $breadcrumbs->push("Details", route('google.users.show'));
});

Breadcrumbs::register('google_groups', function ($breadcrumbs) {
    $breadcrumbs->parent('google');
    $breadcrumbs->push("Groups", route('google.groups.index'));
});
Breadcrumbs::register('google_groups_show', function ($breadcrumbs, $group) {
    $breadcrumbs->parent('google');
    $breadcrumbs->push("Groups", route('google.groups.index'));
    $breadcrumbs->push("Details", route('google.groups.show', $group));
});
Breadcrumbs::register('google_groups_all', function ($breadcrumbs) {
    $breadcrumbs->parent('google');
    $breadcrumbs->push("Groups", route('google.groups.index'));
    $breadcrumbs->push("All", route('google.groups.all'));
});
Breadcrumbs::register('google_groups_create', function ($breadcrumbs) {
    $breadcrumbs->parent('google');
    $breadcrumbs->push("Groups", route('google.groups.index'));
    $breadcrumbs->push("Create New", route('google.groups.create'));
});
Breadcrumbs::register('google_groups_edit', function ($breadcrumbs) {
    $breadcrumbs->parent('google');
    $breadcrumbs->push("Groups", route('google.groups.index'));
    // $breadcrumbs->push("Groups", route('google.groups.key'));
   // $breadcrumbs->push("Edit Settings", route('google.groups.edit'));
});


// Package breadcrumbs
foreach (config('ccps.modules') as $name => $module) {
    require(base_path('vendor/' . $module['package'] . '/src/breadcrumbs/' . $name . '.php'));
}
