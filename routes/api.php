<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->middleware(['auth:sanctum'])->group(function () {
    Route::get('whatis/{query}', 'Api\WhatisController@get');
    Route::prefix('users')->group(function () {
        Route::get('{username}', 'Api\UserController@get');
        Route::get('{username}/aliases', 'Api\UserController@aliases');
        Route::get('{username}/tokens', 'Api\UserController@tokens');
        Route::get('{username}/groups', 'Api\UserController@groups');
        Route::get('{username}/email', 'Api\UserController@emailProfile');
        Route::get('{username}/email/autoforward', 'Api\UserController@emailAutoForward');
        Route::get('{username}/email/delegates', 'Api\UserController@emailDelegates');
        Route::get('{username}/email/filters', 'Api\UserController@emailFilters');
        Route::get('{username}/email/forwards', 'Api\UserController@emailForwards');
        Route::get('{username}/email/labels', 'Api\UserController@emailLabels');
        Route::get('{username}/email/language', 'Api\UserController@emailLanguage');
        Route::get('{username}/email/label/{id}', 'Api\UserController@emailLabelDetails');
        Route::get('{username}/email/imap', 'Api\UserController@emailImap');
        Route::get('{username}/email/pop', 'Api\UserController@emailPop');
        Route::get('{username}/email/sendas', 'Api\UserController@emailSendAs');
        Route::get('{username}/email/vacation', 'Api\UserController@emailVacation');
    });
    Route::prefix('groups')->group(function () {
        Route::get('{group}', 'Api\GroupController@get');
        Route::get('{group}/membership', 'Api\GroupController@listGroupMembers');
        Route::get('{group}/membership/{username}', 'Api\GroupController@hasGroupMember');
        Route::get('{group}/membership/{username}/settings', 'Api\GroupController@getGroupMember');
        Route::get('{group}/settings', 'Api\GroupController@getGroupSettings');
    });
});
